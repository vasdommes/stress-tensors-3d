-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "yale-grace-rse" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/vast/palmer/scratch/poland/rse23/hyperion")
  { emailAddr             = Just "rajeev.erramilli@yale.edu"
  , defaultSbatchOptions  = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "pi_poland" }
  , maxSlurmJobs          = Just 400
  }
