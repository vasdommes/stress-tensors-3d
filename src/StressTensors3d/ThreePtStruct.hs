{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}

module StressTensors3d.ThreePtStruct
  ( ThreePtStruct(..)
  , mapLabel
  , mapOPEStructLabel
  , mapOPEExtStructLabel
  ) where

import Blocks                (Delta (..))
import Blocks.Blocks3d       (ConformalRep (..))
import Bootstrap.Bounds      (OPECoefficient, OPECoefficientExternal,
                              ThreePointStructure (..), mapOPEExtStruct,
                              mapOPEStruct)
import Data.Binary           (Binary)
import GHC.Generics          (Generic)
import StressTensors3d.ToTeX (ToTeX (..))

-- | A three point structure with a general label. TODO: Move to
-- blocks-3d and implement SO3Struct in terms of this?
data ThreePtStruct d t = ThreePtStruct
  { rep1  :: ConformalRep Rational
  , rep2  :: ConformalRep Rational
  , rep3  :: ConformalRep d
  , label :: t
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Binary)

instance ThreePointStructure (ThreePtStruct Delta t) t (ConformalRep Rational) (ConformalRep Delta) where
  makeStructure = ThreePtStruct

instance ThreePointStructure (ThreePtStruct Delta t) t (ConformalRep Rational) (ConformalRep Rational) where
  makeStructure o1 o2 o3 struct = ThreePtStruct o1 o2 o3' struct
    where
      o3' = o3 { delta = Fixed o3.delta }

instance (ToTeX t, ToTeX d) => ToTeX (ThreePtStruct d t) where
  toTeX (ThreePtStruct r1 r2 r3 s) =
    "[" <> toTeX r1 <>"," <> toTeX r2 <> "," <> toTeX r3 <> "|" <> toTeX s <> "]"

mapLabel :: (t -> t') -> ThreePtStruct d t -> ThreePtStruct d t'
mapLabel f s = s { label = f s.label }

-- | Map a function over the struct label of an OPECoefficient
mapOPEStructLabel
  :: (Num a, Eq a, Ord d, Ord t')
  => (t -> t')
  -> OPECoefficient o (ThreePtStruct d t) a
  -> OPECoefficient o (ThreePtStruct d t') a
mapOPEStructLabel = mapOPEStruct . mapLabel

-- | Map a function over the struct label of an OPECoefficientExternal
mapOPEExtStructLabel
  :: (Num a, Eq a, Ord d, Ord t')
  => (t -> t')
  -> OPECoefficientExternal o (ThreePtStruct d t) a
  -> OPECoefficientExternal o (ThreePtStruct d t') a
mapOPEExtStructLabel = mapOPEExtStruct . mapLabel
