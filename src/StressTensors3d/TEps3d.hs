{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TEps3d
  ( TEps3d(..)
  , Objective(..)
  , getMatExt
  ) where

import Blocks                       (BlockFetchContext, Coordinate (XT),
                                     CrossingMat, Delta, Derivative (..),
                                     TaylorCoeff (..), Taylors)
import Blocks.Blocks3d              (ConformalRep (..), Parity (..))
import Blocks.Blocks3d              qualified as B3d
import Bootstrap.Bounds             (BoundDirection, FourPointFunctionTerm,
                                     OPECoefficient, OPECoefficientExternal,
                                     Spectrum, addOPECoeffExt, boundDirSign,
                                     opeCoeffExternalSimple)
import Bootstrap.Bounds             qualified as Bounds
import Bootstrap.Math.FreeVect      (FreeVect, (*^))
import Bootstrap.Math.HalfInteger   (HalfInteger)
import Bootstrap.Math.Linear        (bilinearPair, toV)
import Bootstrap.Math.Linear        qualified as L
import Bootstrap.Math.VectorSpace   (zero)
import Data.Aeson                   (FromJSON, ToJSON)
import Data.Binary                  (Binary)
import Data.Maybe                   (maybeToList)
import Data.Matrix.Static           (Matrix)
import Data.Reflection              (Reifies)
import Data.Tagged                  (Tagged)
import Data.Text qualified          as Text
import Data.Vector                  (Vector)
import Data.Vector                  qualified as Vector
import GHC.Generics                 (Generic)
import Hyperion                     (Dict (..), Static (..))
import Hyperion.Bootstrap.Bound     (BuildInJob, SDPFetchBuildConfig (..),
                                     ToSDP (..), reifyNatFromInt)
import Linear.V                     (V)
import SDPB qualified
import StressTensors3d.ConstantBlock (appendConstantMatrix)
import StressTensors3d.CrossingEqs  (crossingEqsStructSet, structsSSSS,
                                     structsTSSS, structsTTSS, structsTTTS,
                                     structsTTTT)
import StressTensors3d.TSig3d       (TSig3d, tsBuildChain, tsFetchConfig)
import StressTensors3d.TSigEpsSetup (ExternalDims (..), ExternalOp (..),
                                     SymmetrySector (..), TSMixedBlock,
                                     TSMixedStruct, lambdaTTEps, lambda_B,
                                     lambda_F, listSpectrum, opeSS, opeTS,
                                     opeTT, so3, toTSMixedBlock, wardSST)
import StressTensors3d.TTTT3d       (withNonEmptyVec)
import StressTensors3d.Z2Rep        (Z2Rep (..))
import Type.Reflection              (Typeable)

constraintLabel :: Z2Rep -> Parity -> HalfInteger -> Text.Text
constraintLabel z2 parity j =
  Text.pack $ show parity <> "_" <> show j

data TEps3d = TEps3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 4 Rational))
  | ExternalOPEBound (V 4 Rational) BoundDirection
  | ExternalOPEFormBound (Matrix 4 4 Rational)
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type TEpsNumCrossing = 37

-- | Crossing equations for the T-Eps system
crossingEqs
  :: forall s b a . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TEpsNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,T,Eps),       structsTTTS)
  , ((T,T,Eps,Eps),     structsTTSS)
  , ((Eps,T,Eps,T),     structsTTSS)
  , ((Eps,T,Eps,Eps),   structsTSSS)
  , ((Eps,Eps,Eps,Eps), structsSSSS)
  ]

derivsVecTEps3d :: TEps3d -> V TEpsNumCrossing (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecTEps3d f = Bounds.derivsVec crossingEqs <*> pure f.blockParams.nmax

ope
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> concat
    [ opeTT r parity
    , opeSS Eps r parity
    , opeTS Eps r parity
    ]
  Z2Odd  -> []

bulkConstraints
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext TSMixedBlock a m, Reifies s ExternalDims)
  => TEps3d
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints t = pure $ do
  (delta, range, SymmetrySector j parity z2) <- listSpectrum t.spins t.spectrum
  let
    opeCoeffs = ope (ConformalRep delta j) parity z2
    params = (t.blockParams, (t.blockParams, t.blockParams))
  maybeToList $ withNonEmptyVec opeCoeffs $ \opeCoeffs' ->
    Bounds.bootstrapConstraint params (derivsVecTEps3d t) range $
    Bounds.mapBlocks toTSMixedBlock $
    Bounds.crossingMatrix opeCoeffs' (crossingEqs @s)

-- | (lambda_B, lambda_F, <EEE>, <TTE>)
opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s ExternalDims)
  => V 4 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( lambda_B `addOPECoeffExt` wardSST Eps
  , lambda_F `addOPECoeffExt` wardSST Eps
  , opeCoeffExternalSimple Eps Eps Eps (so3 0 0)
  , lambdaTTEps
  )

matExt
  :: forall a s . (Fractional a, Eq a,  Reifies s ExternalDims)
  => Tagged s (CrossingMat 4 TEpsNumCrossing TSMixedBlock a)
matExt =
  pure $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Eps]

getMatExt
  :: (BlockFetchContext TSMixedBlock a m, Applicative m, RealFloat a)
  => TEps3d
  -> m (Matrix 4 4 (Vector a))
getMatExt t =
  Bounds.getIsolatedMat params (derivsVecTEps3d t) (Bounds.runTagged t.externalDims matExt)
  where
    params = (t.blockParams, (t.blockParams, t.blockParams))

matUnit
  :: forall s a . (Reifies s ExternalDims , Fractional a, Eq a)
  => Tagged s (CrossingMat 1 TEpsNumCrossing B3d.IdentityBlock3d a)
matUnit = pure $ B3d.identityCrossingMat (crossingEqs @s)

tEps3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m, BlockFetchContext TSMixedBlock a m)
  => TEps3d
  -> SDPB.SDP m a
tEps3dSDP t = Bounds.runTagged t.externalDims $ do
  bulk <- bulkConstraints t
  unit <- matUnit
  ext <- matExt
  let
    dv = derivsVecTEps3d t
    params = (t.blockParams, (t.blockParams, t.blockParams))
  (cons, obj, norm) <- case t.objective of
    Feasibility mLambda -> pure $ 
      let
        extConstraint = case mLambda of
          Just lambda -> Bounds.isolatedConstraint params dv $
            bilinearPair (fmap fromRational lambda) ext
          Nothing -> Bounds.isolatedConstraint params dv ext
      in
        ( bulk ++ [extConstraint]
        , Bounds.bootstrapObjective t.blockParams dv $ zero `asTypeOf` unit
        , Bounds.bootstrapNormalization t.blockParams dv unit
        )
    -- This is broken, likely because the normalization condition is currently zero
    ExternalOPEBound lambda direction -> pure $ 
      let
        matLambda = boundDirSign direction *^ bilinearPair (fmap fromRational lambda) ext
      in
        ( bulk
        , Bounds.bootstrapObjective t.blockParams dv unit
        , Bounds.bootstrapNormalization params dv matLambda
        )
    ExternalOPEFormBound m -> error "not implemented"--do
      -- let
      --   dv' = dv L.++ toV (Vector.singleton (TaylorCoeff (Derivative (0,0))))
      --   extConstraint' = Bounds.isolatedConstraintLabeled "ext" (params, ()) dv' $
      --     appendConstantMatrix (fmap (negate . fromRational) m) ext
      -- bulk' <- sequence $ do
      --   z2  <- [Z2Even]
      --   parity <- [ParityEven, ParityOdd]
      --   j <- t.spins
      --   (delta, range) <- Bounds.listDeltas (SymmetrySector j parity z2) t.spectrum
      --   let opeCoeffs = ope (ConformalRep delta j) parity z2
      --   maybeToList $ withNonEmptyVec opeCoeffs $ \coeffs ->
      --     Bounds.bootstrapConstraintLabeled (constraintLabel z2 parity j) (params, ()) dv' range .
      --     appendConstantMatrix zero <$>
      --     Bounds.mapBlocks toTSMixedBlock $
      --     Bounds.crossingMatrix opeCoeffs (crossingEqs @s)
      -- pure $
      --   ( bulk' ++ [extConstraint']
      --   , Bounds.bootstrapObjective (t.blockParams, ()) dv' $
      --     appendConstantMatrix zero unit
      --   , Bounds.bootstrapNormalization (t.blockParams, ()) dv' $
      --     appendConstantMatrix (pure 1) (zero `asTypeOf` unit)
      --   )
  pure $ SDPB.SDP obj norm cons

instance ToSDP TEps3d where
  type SDPFetchKeys TEps3d = SDPFetchKeys TSig3d
  toSDP = tEps3dSDP
  reifyPrecisionWithFetchContext _ _ n go = reifyNatFromInt n go

instance SDPFetchBuildConfig TEps3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TEps3d)              where closureDict = static Dict
instance Static (Show TEps3d)                where closureDict = static Dict
instance Static (ToSDP TEps3d)               where closureDict = static Dict
instance Static (ToJSON TEps3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TEps3d) where closureDict = static Dict
instance Static (BuildInJob TEps3d)          where closureDict = static Dict

