{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.TSOStructLabel
  ( TSOStructLabel(..)
  ) where

import Blocks.Blocks3d                (ConformalRep (..), SO3Struct (..),
                                       SO3StructLabel (..))
import Bootstrap.Math.FreeVect        (FreeVect)
import Bootstrap.Math.FreeVect        qualified as FreeVect
import Bootstrap.Math.HalfInteger     (HalfInteger (..))
import Bootstrap.Math.Util            ((^))
import Data.Aeson                     (FromJSON, ToJSON)
import Data.Binary                    (Binary)
import Data.Text                      qualified as Text
import GHC.Generics                   (Generic)
import Hyperion.Static                (Dict (..), Static (..))
import Prelude                        hiding ((^))
import StressTensors3d.BlockRF        (BlockRF (..))
import StressTensors3d.BlockRF        qualified as BlockRF
import StressTensors3d.CompositeBlock (SO3StructVar, ToSO3Structs (..))
import StressTensors3d.ThreePtStruct  (ThreePtStruct (..))
import StressTensors3d.ToTeX          (ToTeX (..))

data TSOStructLabel
  = STOParityEven Int
  | TSOParityEven Int
  | STOParityOdd Int
  | TSOParityOdd Int
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Binary, FromJSON, ToJSON)

instance ToTeX TSOStructLabel where
  toTeX l = "\\textrm{" <> Text.pack (show l) <> "}"

instance ToSO3Structs TSOStructLabel where
  toSO3Structs = toSO3StructsTSOStructLabel

instance Static (Binary TSOStructLabel)       where closureDict = static Dict
instance Static (ToSO3Structs TSOStructLabel) where closureDict = static Dict
instance Static (Show TSOStructLabel)         where closureDict = static Dict

toSO3StructsTSOStructLabel
  :: (Floating a, Eq a)
  => ThreePtStruct () TSOStructLabel
  -> FreeVect SO3StructVar (BlockRF a)
toSO3StructsTSOStructLabel (ThreePtStruct o1 o2 o3 s) =
  FreeVect.mapBasis mkStruct (tsoToSO3StructLabels ds s)
  where
    mkStruct (SO3StructLabel j12 j123) = SO3Struct o1 o2 o3 j12 j123
    ds = case s of
      STOParityEven _ -> o1.delta
      STOParityOdd  _ -> o1.delta
      TSOParityEven _ -> o2.delta
      TSOParityOdd  _ -> o2.delta

-- Automatically generated by toHaskell.nb on Sun 16 Jul 2023 01:06:22
tsoToSO3StructLabels
  :: forall a . (Floating a, Eq a)
  => Rational
  -> TSOStructLabel
  -> FreeVect SO3StructLabel (BlockRF a)
tsoToSO3StructLabels ds' (STOParityEven jInt) = FreeVect.fromList [(SO3StructLabel (2) (-2 + jHalfInteger), BlockRF.fromCoeffsPoles [((-1)^jInt*(ds - j)*(2 + ds - j)*sqrt (1 + j)*sqrt (2 + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j)), (2*(-1)^jInt*sqrt (1 + j)*sqrt (2 + j)*(-1 - ds + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j)), ((-1)^jInt*sqrt (1 + j)*sqrt (2 + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j))] [(jRational, 2)]), (SO3StructLabel (2) (jHalfInteger), BlockRF.fromCoeffsPoles [((-1)^(1 + jInt)*sqrt (2/3)*sqrt (-1 + j)*sqrt (2 + j)*(-ds + j)*(1 + ds + j))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j)), ((-1)^(1 + jInt)*sqrt (2/3)*(1 + 2*ds)*sqrt (-1 + j)*sqrt (2 + j))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j)), ((-1)^jInt*sqrt (2/3)*sqrt (-1 + j)*sqrt (2 + j))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j))] [(jRational, 2)]), (SO3StructLabel (2) (2 + jHalfInteger), BlockRF.fromCoeffsPoles [((-1)^jInt*sqrt (-1 + j)*sqrt (j)*(1 + ds + j)*(3 + ds + j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j)), (-2*(-1)^jInt*sqrt (-1 + j)*sqrt (j)*(2 + ds + j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j)), ((-1)^jInt*sqrt (-1 + j)*sqrt (j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j))] [(jRational, 2)])]
  where
    j            = fromIntegral @Int @a           jInt
    jRational    = fromIntegral @Int @Rational    jInt
    jHalfInteger = fromIntegral @Int @HalfInteger jInt
    ds = fromRational ds'
tsoToSO3StructLabels ds' (STOParityOdd jInt) = FreeVect.fromList [(SO3StructLabel (2) (-1 + jHalfInteger), BlockRF.fromCoeffsPoles [(-1)^jInt*sqrt (2 + j)*(-1 - ds + j), (-1)^jInt*sqrt (2 + j)] [(jRational, 1)]), (SO3StructLabel (2) (1 + jHalfInteger), BlockRF.fromCoeffsPoles [(-1)^(1 + jInt)*sqrt (-1 + j)*(2 + ds + j), (-1)^jInt*sqrt (-1 + j)] [(jRational, 1)])]
  where
    j            = fromIntegral @Int @a           jInt
    jRational    = fromIntegral @Int @Rational    jInt
    jHalfInteger = fromIntegral @Int @HalfInteger jInt
    ds = fromRational ds'
tsoToSO3StructLabels ds' (TSOParityEven jInt) = FreeVect.fromList [(SO3StructLabel (2) (-2 + jHalfInteger), BlockRF.fromCoeffsPoles [(sqrt (1 + j)*sqrt (2 + j)*(-2 + ds + j)*(ds + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j)), (-2*sqrt (1 + j)*sqrt (2 + j)*(-1 + ds + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j)), (sqrt (1 + j)*sqrt (2 + j))/(3*sqrt (-1 + 2*j)*sqrt (1 + 2*j))] [(jRational, 2)]), (SO3StructLabel (2) (jHalfInteger), BlockRF.fromCoeffsPoles [-((sqrt (2/3)*sqrt (-1 + j)*sqrt (2 + j)*(ds - ds^2 + j + j^2))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j))), (sqrt (2/3)*(1 - 2*ds)*sqrt (-1 + j)*sqrt (2 + j))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j)), (sqrt (2/3)*sqrt (-1 + j)*sqrt (2 + j))/(sqrt (-1 + 2*j)*sqrt (3 + 2*j))] [(jRational, 2)]), (SO3StructLabel (2) (2 + jHalfInteger), BlockRF.fromCoeffsPoles [(sqrt (-1 + j)*sqrt (j)*(1 - ds + j)*(3 - ds + j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j)), (2*sqrt (-1 + j)*sqrt (j)*(2 - ds + j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j)), (sqrt (-1 + j)*sqrt (j))/(3*sqrt (1 + 2*j)*sqrt (3 + 2*j))] [(jRational, 2)])]
  where
    j            = fromIntegral @Int @a           jInt
    jRational    = fromIntegral @Int @Rational    jInt
    jHalfInteger = fromIntegral @Int @HalfInteger jInt
    ds = fromRational ds'
tsoToSO3StructLabels ds' (TSOParityOdd jInt) = FreeVect.fromList [(SO3StructLabel (2) (-1 + jHalfInteger), BlockRF.fromCoeffsPoles [sqrt (2 + j)*(-1 + ds + j), -sqrt (2 + j)] [(jRational, 1)]), (SO3StructLabel (2) (1 + jHalfInteger), BlockRF.fromCoeffsPoles [(-2 + ds - j)*sqrt (-1 + j), -sqrt (-1 + j)] [(jRational, 1)])]
  where
    j            = fromIntegral @Int @a           jInt
    jRational    = fromIntegral @Int @Rational    jInt
    jHalfInteger = fromIntegral @Int @HalfInteger jInt
    ds = fromRational ds'
