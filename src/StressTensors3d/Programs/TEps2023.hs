{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PatternSynonyms     #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Programs.TEps2023 where

import Blocks.Blocks3d                    qualified as B3d
import Bootstrap.Bounds                   (BoundDirection (..))
import Bootstrap.Bounds.Spectrum          qualified as Spectrum
import Bootstrap.Math.AffineTransform     (AffineTransform (..))
import Bootstrap.Math.DampedRational      qualified as DR
import Bootstrap.Math.Linear              (pattern V2, toM, toV,  fromV)
import Bootstrap.Math.Util                (dot)
import Control.Monad.IO.Class             (liftIO)
import Control.Monad.Reader               (asks, local, void)
import Data.Map                           qualified as Map
import Data.Matrix.Static                 (Matrix)
import Data.Proxy                         (Proxy (..))
import Data.Scientific                    (Scientific)
import Data.Scientific                    qualified as Scientific
import Data.Tagged                        (untag)
import Data.Text                          (Text)
import Hyperion
import Hyperion.Bootstrap.Bound           (Bound (..), CheckpointMap, LambdaMap)
import Hyperion.Bootstrap.Bound           qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch  (delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Main            (unknownProgram)
import Hyperion.Bootstrap.OPESearch       (BilinearForms (..),
                                           OPESearchConfig (..))
import Hyperion.Bootstrap.OPESearch       qualified as OPE
import Hyperion.Bootstrap.Params          qualified as Params
import Hyperion.Database                  qualified as DB
import Hyperion.Log                       qualified as Log
import Hyperion.Util                      (hour)
import Linear.V                           (V)
import SDPB qualified
import StressTensors3d.Programs.Defaults  (defaultBoundConfig,
                                           defaultDelaunayConfig,
                                           defaultQuadraticNetConfig)
import StressTensors3d.Programs.TestMixed (z2EvenParityEvenScalar,
                                           z2EvenParityOddScalar,
                                           z2OddParityEvenScalar)
import StressTensors3d.TEps3d          (TEps3d (..))
import StressTensors3d.TEps3d          qualified as TEps3d
import StressTensors3d.TSigEpsSetup       (ExternalDims (..))
import StressTensors3d.TSigEpsSetup       qualified as TSigEpsSetup
import StressTensors3d.Z2Rep              (Z2Rep (..))

z2EvenParityEvenSpin2 :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenSpin2 = TSigEpsSetup.SymmetrySector 2 B3d.ParityEven Z2Even

deltaVToExternalDims :: V 2 Rational -> ExternalDims
deltaVToExternalDims (V2 deltaSig deltaEps) =
  ExternalDims { deltaSig = deltaSig, deltaEps = deltaEps }

externalDimsToDeltaV :: ExternalDims -> V 2 Rational
externalDimsToDeltaV dims = V2 dims.deltaSig dims.deltaEps

boundDeltaV :: Bound prec TEps3d -> V 2 Rational
boundDeltaV bound = externalDimsToDeltaV bound.boundKey.externalDims

tSigEpsNoGaps :: V 2 Rational -> Maybe (V 4 Rational) -> Int -> TEps3d
tSigEpsNoGaps deltaExt mLambdaTTT nmax = TEps3d
  { spectrum     = Spectrum.unitarySpectrum
  , objective    = TEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tEpsFeasibleDefaultGaps :: V 2 Rational -> Maybe (V 4 Rational) -> Int -> TEps3d
tEpsFeasibleDefaultGaps deltaExt mLambdaTTT nmax = TEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    -- , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tEpsTTeBoundGaps :: V 2 Rational -> Int -> TEps3d
tEpsTTeBoundGaps deltaExt nmax = TEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, deltaEps+1e-6 )
    -- , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    -- , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TEps3d.ExternalOPEFormBound tteForm
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }
  where
    (_,deltaEps) = fromV deltaExt
    tteForm = toM
      ((0,0,0,0)
      ,(0,0,0,0)
      ,(0,0,0,0)
      ,(0,0,0,1))



getExtMat :: Bound Int TEps3d -> Bound.BoundFiles -> Job (Matrix 4 4 Scientific)
getExtMat bound' files = untag @Job $
  Bound.reifyBoundWithContext bound' $ \(bound :: Bound (Proxy p) TEps3d) -> do
  opeMatrix <- Bound.getBoundObject bound files TEps3d.getMatExt
  norm <- Bound.getBoundObject bound files $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alpha <- liftIO $ SDPB.readFunctional @(Bound.BigFloat p) norm files.outDir
  pure $
    fmap Scientific.fromFloatDigits $
    fmap (alpha `dot`) opeMatrix

logExtMatLocal :: Bound Int TEps3d -> Bound.BoundFiles -> Cluster ()
logExtMatLocal bound files = do
  progInfo <- asks clusterProgramInfo
  extMat <- liftIO $ runJobLocal progInfo $ getExtMat bound files
  Log.info "External matrix" extMat

coerceBase :: DR.DampedRational base1 f a -> DR.DampedRational base2 f a
coerceBase DR.DampedRational{..} = DR.DampedRational{..}

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

boundsProgram :: Text -> Cluster ()

boundsProgram "TEps_test_nmax6" =
  local (setJobType (MPIJob 1 127) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "large-shared") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ -- An initial guess for the Ising location: The Ising dimensions
    -- and OPE coefficients sse,eee are from
    -- https://arxiv.org/abs/1603.04436
    --
    -- For lambdaB, lambdaF, we used the known cT, and the estimate
    -- lambdaF/lambdaB = tan theta, with theta = 0.015 (roughly the
    -- center of the regions in figure 17 of
    -- https://arxiv.org/pdf/1708.05718.pdf)
    --
    -- For TTe, we estimated based on Rajeev's plot

    -- This point is ruled out:
    -- (toV (0.5181489, 1.412625), Just $ toV (1.01266, 0.01519, 1.0518537, 1.532435, 0.95))

    -- -- This point is allowed!
    -- (toV (0.5181489, 1.412625), Nothing)
    (toV (0.5181489, 1.412625), Just $ toV (-0.436525, -0.00614752, -0.660002, -0.410594))
    -- This point is disallowed
    -- (toV (0.517,1.45), Nothing)
    -- ,  (toV (0.518144590625, 1.41266015625), Just $ toV (-0.436525, -0.00614752, -0.660002, -0.410594))
  ]
  where
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tEpsFeasibleDefaultGaps dExt mLambda nmax)
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TEps_TTe_bound_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (4*hour) . setJobMemory "128G" . setSlurmPartition "shared") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  -- [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  -- [(x,y) | x <- [3,3.025..4.5], y <- [1.95,1.975..7]]
  -- [toV (0.51,y) | y <- [1,1.412625,1.9]]
  [toV (0.51,y) | y <- [6/10,65/100..2.5]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = tEpsTTeBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram p = unknownProgram p

