{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Programs.Test where

import Blocks.Blocks3d                   qualified as B3d
import Blocks.Delta                      (Delta (..))
import Bootstrap.Bounds.BoundDirection   (BoundDirection (..))
import Bootstrap.Bounds.Spectrum         qualified as Spectrum
import Bootstrap.Math.AffineTransform    (AffineTransform (..))
import Bootstrap.Math.Linear             (fromV, toV)
import Bootstrap.Math.VectorSpace        qualified as V
import Control.Monad.Reader              (asks, liftIO, local, void)
import Data.Aeson                        (ToJSON)
import Data.Binary                       (Binary)
import Data.Map.Strict                   qualified as Map
import Data.Ratio                        (approxRational)
import Data.Tagged                       (untag)
import Data.Text                         (Text)
import Data.Time.Clock                   (NominalDiffTime)
import GHC.Generics                      (Generic)
import Hyperion
import Hyperion.Bootstrap.BinarySearch   (BinarySearchConfig (..), Bracket (..))
import Hyperion.Bootstrap.Bound          (Bound (..))
import Hyperion.Bootstrap.Bound          qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..),
                                          delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Main           (unknownProgram)
import Hyperion.Bootstrap.OPESearch      qualified as OPE
import Hyperion.Bootstrap.Params         qualified as Params
import Hyperion.Database                 qualified as DB
import Hyperion.Log                      qualified as Log
import Hyperion.Util                     (day, hour)
import Linear.V                          (V)
import SDPB qualified
import StressTensors3d.Programs.Defaults (defaultBoundConfig,
                                          defaultDelaunayConfig)
import StressTensors3d.TTTT3d            (TTTT3d (..))
import StressTensors3d.TTTT3d            qualified as TTTT3d

data TTTT3dBinarySearch = TTTT3dBinarySearch
  { stress_bs_bound     :: Bound Int TTTT3d
  , stress_bs_config    :: BinarySearchConfig Rational
  , stress_bs_gapSector :: TTTT3d.SymmetrySector
  } deriving (Show, Generic, Binary, ToJSON)

remoteTTTT3dBinarySearch :: TTTT3dBinarySearch -> Cluster (Bracket Rational)
remoteTTTT3dBinarySearch stress_bs = Bound.remoteBinarySearchBoundWithHotStarting Bound.MkBinarySearchBound
  { Bound.bsBoundClosure      = static mkBound `ptrAp` cPure (stress_bs_bound stress_bs) `cAp` cPure (stress_bs_gapSector stress_bs)
  , Bound.bsConfig            = stress_bs_config stress_bs
  , Bound.bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound bound gapSector gap =
      bound { boundKey = (boundKey bound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = Spectrum.setGap gapSector gap (spectrum (boundKey bound))
    getBool _ _ = pure <$> SDPB.isPrimalFeasible

evenScalar, oddScalar, evenSpin2 :: TTTT3d.SymmetrySector
evenScalar = TTTT3d.SymmetrySector 0 B3d.ParityEven
oddScalar = TTTT3d.SymmetrySector 0 B3d.ParityOdd
evenSpin2 = TTTT3d.SymmetrySector 2 B3d.ParityEven

boundToScalarGapV :: Bound prec TTTT3d -> V 2 Rational
boundToScalarGapV Bound { boundKey = TTTT3d { spectrum = s } } =
  toV (deltaEven, deltaOdd)
  where
    toScalarDim c = case Spectrum.deltaGap s c of
      Fixed d             -> d
      RelativeUnitarity d -> d + 1/2
    deltaEven = toScalarDim evenScalar
    deltaOdd  = toScalarDim oddScalar

ttttNoGaps :: Int -> TTTT3d
ttttNoGaps nmax = TTTT3d
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.Feasibility Nothing
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttFeasibleDefaultGaps :: Maybe (V 2 Rational) -> Int -> TTTT3d
ttttFeasibleDefaultGaps mLambdaTTT nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, 7)
    , (oddScalar,  7)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttFeasibleTestGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttFeasibleTestGaps (dEven,dOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, dEven)
    , (oddScalar,  dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.Feasibility Nothing
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttFeasibleSearchGaps :: V 2 Rational -> Int -> TTTT3d
ttttFeasibleSearchGaps v nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, dEven)
    , (oddScalar,  dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.Feasibility Nothing
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (dEven,dOdd) = fromV v

ttttCTBoundGaps :: V 2 Rational -> BoundDirection -> Int -> TTTT3d
ttttCTBoundGaps lambdaTTT direction nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, 6)
    , (oddScalar,  7)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTeBoundGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttTTeBoundGaps (deltaEven,deltaOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven+1e-6)
    , (oddScalar, deltaOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBound deltaEven UpperBound
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTeScalarBoundGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttTTeScalarBoundGaps (deltaEven,deltaOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven+1e-6)
    , (oddScalar, deltaOdd)
    , (evenSpin2, 3.5)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBoundWithLambda deltaEven UpperBound (toV (1,0))
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTeFermionBoundGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttTTeFermionBoundGaps (deltaEven,deltaOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven+1e-6)
    , (oddScalar, deltaOdd)
    , (evenSpin2, 3.5)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBoundWithLambda deltaEven UpperBound (toV (0,1))
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTeBothBoundGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttTTeBothBoundGaps (deltaEven,deltaOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven+1e-6)
    , (oddScalar, deltaOdd)
    -- , (evenSpin2, 3.5)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBoundWithLambda deltaEven UpperBound (toV (1,1))
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTeWithLambdaBoundGaps :: (Rational,Rational) -> (V 2 Rational) -> Int -> TTTT3d
ttttTTeWithLambdaBoundGaps (deltaEven,deltaOdd) lambda nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven+1e-6)
    , (oddScalar, deltaOdd)
    -- , (evenSpin2, 3.5)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBoundWithLambda deltaEven UpperBound lambda
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTsBoundGaps :: (Rational,Rational) -> Int -> TTTT3d
ttttTTsBoundGaps (deltaEven,deltaOdd) nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven)
    , (oddScalar, deltaOdd+1e-6)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTsOPEBound deltaOdd UpperBound
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttIsingTTeBoundGaps :: (Rational,Rational) -> BoundDirection -> Int -> TTTT3d
ttttIsingTTeBoundGaps (deltaEven,deltaEvenGap) dir nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEvenGap)
    , (oddScalar, 6)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTeOPEBound deltaEven dir
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttTTsOdd'BoundGaps :: (Rational,Rational) -> BoundDirection -> Int -> TTTT3d
ttttTTsOdd'BoundGaps (deltaOdd,deltaOddGap) dir nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (oddScalar, deltaOddGap)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.TTsOPEBound deltaOdd dir
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttCTScalarBoundGaps :: (Rational,Rational) -> V 2 Rational -> BoundDirection -> Int -> TTTT3d
ttttCTScalarBoundGaps (deltaEven,deltaOdd) lambdaTTT direction nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, deltaEven)
    , (oddScalar,  deltaOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }


ttttCTOddBoundGaps :: Rational -> V 2 Rational -> BoundDirection -> Int -> TTTT3d
ttttCTOddBoundGaps deltaOdd lambdaTTT direction nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [
     (oddScalar,  deltaOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

-- keep this as a souvenir of the zeros of the minors of our matrix
ttttCrazyGaps :: Maybe (V 2 Rational) -> Int -> TTTT3d
ttttCrazyGaps mLambdaTTT nmax = TTTT3d
  { spectrum     = Spectrum.setTwistGap 193 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }

ttttFixedOPE :: (Rational,Rational) -> V 2 Rational -> Int -> TTTT3d
ttttFixedOPE (dEven,dOdd) lambdaTTT nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, dEven)
    , (oddScalar,  dOdd)
    , (evenSpin2, 4)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.FeasibilityFixedOPE lambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }


ttttCTIsingGaps :: Rational -> V 2 Rational -> BoundDirection -> Int -> TTTT3d
ttttCTIsingGaps dOdd lambdaTTT direction nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [ (evenScalar, 1.2)
    , (oddScalar, dOdd)
    ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.addIsolated evenScalar deltaSigInternal Spectrum.unitarySpectrum
  , objective    = TTTT3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    deltaSigInternal = 0.51820


ttttCTUnitarityGaps :: V 2 Rational -> BoundDirection -> Int -> TTTT3d
ttttCTUnitarityGaps lambdaTTT direction nmax = TTTT3d
  { spectrum     = Spectrum.setGaps
    [] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TTTT3d.StressTensorOPEBound lambdaTTT direction
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

data TTTTDelaunaySearchData = TTTTDelaunaySearchData
  { nmax              :: Int
  , affine            :: AffineTransform 2 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 2 Rational]
  , initialAllowed    :: [V 2 Rational]
  , solverPrecision   :: Int
  , delaunayConfig    :: DelaunayConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  , slurmPartition    :: Text
  }

ttttDelaunaySearch :: TTTTDelaunaySearchData -> Cluster ()
ttttDelaunaySearch t =
  local (setJobType t.jobType . setJobTime t.jobTime . setJobMemory t.jobMemory . setSlurmPartition t.slurmPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap t.affine boundToScalarGapV t.initialCheckpoint
  let
    f v =  do
      result <- Bound.remoteComputeWithCheckpointMap checkpointMap (bound v)
      return (SDPB.isPrimalFeasible result)
  delaunaySearchRegionPersistent delaunaySearchPoints t.delaunayConfig t.affine initialPts f
  where
    bound v = Bound
      { boundKey = ttttFeasibleSearchGaps v t.nmax
      , precision = t.solverPrecision
      , solverParams = (Params.jumpFindingParams t.nmax) { SDPB.precision = t.solverPrecision }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- t.initialAllowed] ++
      [(p, Just False) | p <- t.initialDisallowed]

ttttDelaunaySearchNmax14 :: TTTTDelaunaySearchData -> Cluster ()
ttttDelaunaySearchNmax14 t =
  local (setJobType t.jobType . setJobTime t.jobTime . setJobMemory t.jobMemory . setSlurmPartition t.slurmPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap t.affine boundToScalarGapV t.initialCheckpoint
  let
    f v =  do
      result <- Bound.remoteComputeWithCheckpointMap checkpointMap (bound v)
      return (SDPB.isPrimalFeasible result)
  delaunaySearchRegionPersistent delaunaySearchPoints t.delaunayConfig t.affine initialPts f
  where
    bound v = Bound
      { boundKey = ( ttttFeasibleSearchGaps v (t.nmax+4) ) { blockParams = (Params.block3dParamsNmax (t.nmax+4)) { B3d.nmax = t.nmax } }
      , precision = t.solverPrecision
      , solverParams = (Params.jumpFindingParams (t.nmax + 4) ) { SDPB.precision = t.solverPrecision }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- t.initialAllowed] ++
      [(p, Just False) | p <- t.initialDisallowed]


boundsProgram :: Text -> Cluster ()

-----------------
--- nmax = 6 ----
-----------------

-- | Make sure you have the right checksum! And then you won't have to call this :)
-- | here's an sha256 checksum:
-- | 46a742ca0d76901b197874f54d19b9528828462c75f574256592b6ca09ac73da  tttt_blocks_nmax6.tar.zst
boundsProgram "TTTT_build_blocks_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "large-shared") $
  void (Bound.remoteCompute bound)
  where
    nmax = 6
    bound = Bound
      { boundKey = ttttNoGaps nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_crazy_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (4*hour) . setJobMemory "50G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (7,7)
  ]
  where
    nmax = 6
    bound lambdaTTT = Bound
      { boundKey = ttttCrazyGaps (Just lambdaTTT) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (24*hour) . setJobMemory "100G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (10,10)
  ]
  where
    nmax = 6
    bound lambdaTTT = Bound
      { boundKey = ttttFeasibleDefaultGaps (Just lambdaTTT) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_gap_test_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (1*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ f
  [ (x,y) | x <- [0.6,0.8..8], y <- [0.6,0.8..12]
  ]
  where
    gridDB :: DB.KeyValMap (V 2 Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f p = do
      out <- Bound.remoteCompute (bound p)
      Log.info "point finished" out
      DB.insert gridDB (toV p) (SDPB.isPrimalFeasible out)
      return ()
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttFeasibleTestGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 768, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_test_functional_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (2*hour) . setJobMemory "50G" . setSlurmPartition "pi_poland,scavenge") $ do
  (_, boundFiles) <- Bound.remoteComputeKeepFiles bound
  let
    myJob = untag @Job $
      Bound.reifyBoundWithContext bound $ \bound' -> do
      functional <- Bound.getBoundFunctional bound' boundFiles
      Log.info "functional" functional
      DB.insert funcDB bound functional
  pInfo <- asks toProgramInfo
  liftIO $ runJobLocal pInfo myJob
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 6
    bound = Bound
      { boundKey = ttttFeasibleDefaultGaps (Just $ toV (1,1)) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 1024, SDPB.dualErrorThreshold = 1e-50 , SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_OPE_test_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (24*hour) . setJobMemory "100G") $
  mapM_ (Bound.remoteCompute . bound)
  [ OPE.thetaVectorApprox 1e-16 x | x <- [approxRational 1e-16 (pi/8 * n) | n <-[0..4 :: Double]]
  ]
  where
    nmax = 6
    bound lambdaTTT = Bound
      { boundKey = ttttFeasibleDefaultGaps (Just lambdaTTT) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TTTT_OPE_Ising_test_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (24*hour) . setJobMemory "20G") $
  mapConcurrently_ f
  [ (dir, dOdd, approxRational (pi/8 * n) 1e-16) | dir <- [UpperBound, LowerBound], dOdd <- [8], n <-[0.05,015..4 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, dOdd, theta) = do
      out <- Bound.remoteCompute (bound direction (dOdd, theta))
      Log.info "point finished" out
      DB.insert gridDB (direction, dOdd, theta) (SDPB.isFinished out)
      return ()
    nmax = 6
    bound dir (dOdd, theta) = Bound
      { boundKey = ttttCTIsingGaps dOdd lambda dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)



boundsProgram "TTTT_OPE_Unitarity_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (24*hour) . setJobMemory "40G") $
  mapConcurrently_ f
  [ (dir, approxRational (pi/8 * (n)) 1e-16) | dir <- [UpperBound,LowerBound], n <- [(-1.95), (-1.85)..0:: Double] ++ [0.05, 0.15..8:: Double] --n <-[(-0.05),(-0.04)..(-0.01) :: Double] ++ [4.01,4.02..4.05 :: Double] ++ [4.10, 4.20..6 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, theta) = do
      out <- Bound.remoteCompute (bound direction theta)
      Log.info "point finished" out
      DB.insert gridDB (direction, theta) (SDPB.isFinished out)
      return ()
    nmax = 6
    bound dir theta = Bound
      { boundKey = ttttCTUnitarityGaps lambda dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)




boundsProgram "TTTT_OPE_OddGap_test_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (24*hour) . setJobMemory "40G") $
  mapConcurrently_ f
  [ (dir, deltaOdd, approxRational (pi/8 * (n)) 1e-16) | dir <- [UpperBound,LowerBound], deltaOdd <- [10 :: Rational], n <- [0.1,0.11..0.14:: Double] --n <-[(-0.05),(-0.04)..(-0.01) :: Double] ++ [4.01,4.02..4.05 :: Double] ++ [4.10, 4.20..6 :: Double]
  ]
  where
    gridDB :: DB.KeyValMap (BoundDirection, Rational, Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    f (direction, deltaOdd, theta) = do
      out <- Bound.remoteCompute (bound direction deltaOdd theta)
      Log.info "point finished" out
      DB.insert gridDB (direction, deltaOdd, theta) (SDPB.isFinished out)
      return ()
    nmax = 6
    bound dir deltaOdd theta = Bound
      { boundKey = ttttCTOddBoundGaps deltaOdd lambda dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = let v = ( OPE.thetaVectorApprox 1e-16 theta) in ( 1/(sum v) V.*^ v)

boundsProgram "TTTT_CT_test_nmax6" =
  local (setJobType (MPIJob 2 18) . setJobTime (1*hour) . setJobMemory "20G" ) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (1,1), toV (3,3)
  ]
  where
    nmax = 6
    bound lambdaTTT = Bound
      { boundKey = ttttCTBoundGaps lambdaTTT UpperBound nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

-- | Compute a lower bound on cT at theta = pi/4, assuming only
-- unitarity (with a twist gap 1e-6.
--
-- result for commit: 35e7c8015f008cf1cfec6ef11e90c5f906c4a4bc
-- objective = -2.121807231385472935723...
--
boundsProgram "TTTT_CT_test_nmax6_DSD" =
  local (setJobType (MPIJob 1 16) . setJobTime (8*hour)) $
  mapConcurrently_ (Bound.remoteComputeKeepFiles . bound) $
  [ lambda (1/4) ]
  where
    lambda :: Rational -> V 2 Rational
    lambda x =
      let
        th = fromRational @Double x * pi
      in
        fmap (flip approxRational 1e-16) $
        toV (cos th / (cos th + sin th), sin th / (cos th + sin th))
    nmax = 6
    bound lambdaTTT = Bound
      { boundKey = ttttCTUnitarityGaps lambdaTTT UpperBound nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_CT_functional_test_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (1*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ (\(dir,thresh) -> do
      (_, boundFiles) <- Bound.remoteComputeKeepFiles (bound (dir,thresh))
      let
        myJob = untag @Job $
          Bound.reifyBoundWithContext (bound (dir,thresh)) $ \bound' -> do
          functional <- Bound.getBoundFunctional bound' boundFiles
          Log.info "functional" functional
          DB.insert funcDB (dir,thresh) functional
      pInfo <- asks toProgramInfo
      liftIO $ runJobLocal pInfo myJob)
    [ (dir,thresh) | dir <- [UpperBound,LowerBound], thresh <- [1e-50,1e-55,1e-60,1e-65,1e-70,1e-75,1e-80]
    ]
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 6
    bound (dir,thresh)  = Bound
      { boundKey = ttttCTBoundGaps (toV (1,1)) dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
        { SDPB.precision = 768
        , SDPB.dualityGapThreshold = 1e-200
        , SDPB.findDualFeasible = True
        , SDPB.dualErrorThreshold = thresh  }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_fixed_OPE_test_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (4*hour) . setJobMemory "30G" . setSlurmPartition "pi_poland,scavenge") $
  void $ (Bound.remoteCompute bound)
  where
    nmax = 10
    bound = Bound
      { boundKey = ttttFixedOPE (7,7) (toV (1,1)) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_delaunay_nmax6" =
  ttttDelaunaySearch $
    TTTTDelaunaySearchData
    { nmax              = 6
    , affine            = AffineTransform
                            { affineShift = toV (4.25,7.25)
                            , affineLinear = toV  ( toV (3.75, 0)
                                                  , toV (0, 6.75) )
                            }
    , initialCheckpoint = Nothing
    , initialDisallowed = toV <$> [(7,7),(8,10),(8,3), (2,10),(8,1/2)]
    , initialAllowed    = toV <$> [(1/2,1/2),(3,3),(6,7),(1,11),(6,2),(2,7)]
    , solverPrecision   = 768
    , delaunayConfig    = defaultDelaunayConfig 8 200
    , jobTime           = 3*hour
    , jobType           = MPIJob 1 9
    , jobMemory         = "30G"
    , slurmPartition    = "pi_poland,scavenge"
    }

boundsProgram "TTTT_spin_test_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (4*hour) . setJobMemory "10G" . setSlurmPartition "pi_poland") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ 0..21 :: Int ]
  where
    nmax = 6
    bound lMax = Bound
      { boundKey = (ttttNoGaps nmax) { spins = fromIntegral <$> [0..lMax]}
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_binary_search_test_nmax6" =
  mapConcurrently_ search $
  [ ((oddScalar,x),(evenScalar,1/2,8)) | x <- [1,1.1..13] ] ++ [ ((evenScalar,x),(oddScalar,1/2,13)) | x <- [1,1.1..8] ]
  where
    jobType = MPIJob 1 6
    jobTime = 2*hour
    prec = 768
    nmax = 6
    search ((chanFixed,deltaFixed),(chanSearched,deltaMinSearched,deltaMaxSearched)) =
      local (setJobType jobType . setJobTime jobTime . setSlurmPartition "scavenge" . setSlurmQos "nothrottle") $
      remoteTTTT3dBinarySearch $
      TTTT3dBinarySearch
      { stress_bs_bound = Bound
        { boundKey = (ttttNoGaps nmax)
          { spectrum = Spectrum.setGap chanFixed deltaFixed $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum}
        , precision = prec
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , stress_bs_gapSector = chanSearched
      , stress_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = deltaMinSearched
          , falsePoint = deltaMaxSearched
          }
        , threshold  = 1e-2
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }

boundsProgram "TTTT_binary_search_cT_nmax6" =
  mapConcurrently_ search $
  [ ((oddScalar,x),(evenScalar,1/2,8))  | x <- [1,1.1..13] ] ++ [ ((evenScalar,x),(oddScalar,1/2,13)) | x <- [1,1.1..8] ]
  where
    jobType = MPIJob 1 6
    jobTime = 2*hour
    prec = 768
    nmax = 6
    search ((chanFixed,deltaFixed),(chanSearched,deltaMinSearched,deltaMaxSearched)) =
      local (setJobType jobType . setJobTime jobTime . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
      remoteTTTT3dBinarySearch $
      TTTT3dBinarySearch
      { stress_bs_bound = Bound
        { boundKey = (ttttNoGaps nmax)
          { spectrum = Spectrum.setGap chanFixed deltaFixed $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
          , objective = TTTT3d.FeasibilityLowerBoundOPE (toV (10,10))}
        , precision = prec
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , stress_bs_gapSector = chanSearched
      , stress_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = deltaMinSearched
          , falsePoint = deltaMaxSearched
          }
        , threshold  = 1e-2
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }


boundsProgram "TTTT_TTe_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  -- [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  -- [(x,y) | x <- [3,3.025..4.5], y <- [1.95,1.975..7]]
  [(x,y) | x <- [1.5,1.525..2.975], y <- [1.95,1.975..7]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttTTeBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_scalar_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  -- [(x,y) | x <- [3.7,3.725..6.1], y <- [1.95,2..7.1]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttTTeScalarBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_fermion_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  -- [(x,y) | x <- [3.7,3.725..6.1], y <- [1.95,2..7.1]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttTTeFermionBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_both_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(x,y) | x <- [0.6,0.7..3], y <- [0.5]]
  -- [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  -- [(x,y) | x <- [3.7,3.725..6.1], y <- [1.95,2..7.1]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttTTeBothBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_lambda_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (1*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound) $
    [((x,y),pi'/2*z) | x <- [0.6,0.7..7], y <- [0.6,0.7..8], z <- [1/8,1/4,3/8,5/8,3/4,7/8]] ++ [((x,y),pi'/4*z) | x <- [0.6,0.7..2], y <- [8,8.1..12], z <- [1/8,1/4,3/8,5/8,3/4,7/8]]
  -- [(x,y) | x <- [3.7,3.725..6.1], y <- [1.95,2..7.1]]
  where
    pi' = approxRational (pi :: Double) 1e-16
    nmax = 6
    bound (deltas,theta) = Bound
      { boundKey = ttttTTeWithLambdaBoundGaps deltas (OPE.thetaVectorApprox 1e-16 theta) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTs_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [(x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttTTsBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_nb_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttCTScalarBoundGaps deltas (toV (1,0)) UpperBound nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_nf_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (x,y) | x <- [0.6,0.7..8], y <- [0.6,0.7..13]]
  where
    nmax = 6
    bound deltas = Bound
      { boundKey = ttttCTScalarBoundGaps deltas (toV (0,1)) UpperBound nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_ising_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (1*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [((x,x+y),dir) | x <- [0.6,0.65..6.5], y <- [0.1,0.2..7], dir <- [UpperBound]]
  where
    nmax = 6
    bound (deltas,dir) = Bound
      { boundKey = ttttIsingTTeBoundGaps deltas dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTs_oddp_bound_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (1*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  -- [((x,x+y),dir) | x <- [0.6,0.65..6.5], y <- [0.1,0.2..7] , dir <- [UpperBound]]
  [((x,x+y),dir) | x <- [0.6,0.65..6.5], y <- [7.1,7.2..14] , dir <- [UpperBound]]
  where
    nmax = 6
    bound (deltas,dir) = Bound
      { boundKey = ttttTTsOdd'BoundGaps deltas dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }


------------------
--- nmax = 10 ----
------------------

-- | 23787d361eedf179cccac59358e04ba161d55e805546c6b5868bf753d354cc41  tttt_blocks_nmax10.tar.zst
boundsProgram "TTTT_test_nmax10" =
  local (setJobType (MPIJob 4 36) . setJobTime (4*hour) . setJobMemory "0G" . setSlurmPartition "pi_poland") $
  void (Bound.remoteCompute bound)
  where
    nmax = 10
    bound = Bound
      { boundKey = ttttFeasibleDefaultGaps Nothing nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_CT_functional_test_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "pi_poland,scavenge") $
  mapConcurrently_ (\(dir,thresh) -> do
      (_, boundFiles) <- Bound.remoteComputeKeepFiles (bound (dir,thresh))
      let
        myJob = untag @Job $
          Bound.reifyBoundWithContext (bound (dir,thresh)) $ \bound' -> do
          functional <- Bound.getBoundFunctional bound' boundFiles
          Log.info "functional" functional
          DB.insert funcDB (dir,thresh) functional
      pInfo <- asks toProgramInfo
      liftIO $ runJobLocal pInfo myJob)
    [ (dir,thresh) | dir <- [UpperBound,LowerBound], thresh <- [1e-50,1e-55,1e-60,1e-65,1e-70,1e-75,1e-80]
    ]
  where
    funcDB = DB.KeyValMap "functional"
    nmax = 10
    bound (dir,thresh)  = Bound
      { boundKey = ttttCTBoundGaps (toV (1,1)) dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
        { SDPB.precision = 1024
        , SDPB.dualityGapThreshold = 1e-200
        , SDPB.findDualFeasible = True
        , SDPB.dualErrorThreshold = thresh  }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_gap_test_nmax10" =
  local (setJobType (MPIJob 1 9) . setJobTime (2*hour) . setJobMemory "30G" . setSlurmPartition "scavenge") $ do
    checkpointMap <- Bound.newCheckpointMap affine deltaExtToV $ Just "/vast/palmer/scratch/poland/rse23/hyperion/data/2023-05/RBwBJ/Object_b8mbkoCsUcrG9erhTI9Me3swFMLOQz3pM4LbIZOS9TI/ck"
    let
      f p = do
        out <- Bound.remoteComputeWithCheckpointMap checkpointMap (bound p)
        Log.info "point finished" out
        DB.insert gridDB (toV p) (SDPB.isPrimalFeasible out)
        return ()
    mapConcurrently_ f
      [ (x,y) | x <- [0.6,0.8..8], y <- [0.6,0.8..12] ]
  where
    affine = AffineTransform
      { affineShift = toV @1 (0)
      , affineLinear = toV @1 ( toV @1 (1) )
      }
    deltaExtToV _ = toV @1 1
    gridDB :: DB.KeyValMap (V 2 Rational) Bool
    gridDB = DB.KeyValMap "gridPoints"
    nmax = 10
    bound deltas = Bound
      { boundKey = ttttFeasibleTestGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 768, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_OPE_test_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "100G") $
  mapM_ (Bound.remoteCompute . bound)
  [ toV (1,1)
  ]
  where
    nmax = 10
    bound lambdaTTT = Bound
      { boundKey = ttttFeasibleDefaultGaps (Just lambdaTTT) nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_delaunay_nmax10" =
  ttttDelaunaySearch $
    TTTTDelaunaySearchData
    { nmax              = 10
    , affine            = AffineTransform
                            { affineShift = toV (4.25,7.25)
                            , affineLinear = toV  ( toV (3.75, 0)
                                                  , toV (0, 6.75) )
                            }
    , initialCheckpoint = Nothing
    , initialDisallowed = toV <$> [(1/2,12),(1,12),(2,12),(2,11),(2,10),(2,9),(3,9),(4,9),(5,9),(6,9),(7,9),(7,8),(7,7),(7,6),(7,5),(7,4),(7,3),(7,2),(8,2),(8,1),(8,1/2)]
    , initialAllowed    = toV <$> [(1/2,11),(1,11),(1,10),(1,9),(1,8),(1,7),(2,7),(3,7),(4,7),(5,7),(6,7),(6,6),(6,5),(6,4),(6,3),(6,2),(6,1),(6,1/2)]
    , solverPrecision   = 768
    , delaunayConfig    = defaultDelaunayConfig 8 600
    , jobTime           = 3*hour
    , jobType           = MPIJob 1 36
    , jobMemory         = "0"
    , slurmPartition    = "pi_poland"
    }

boundsProgram "TTTT_spin_test_nmax10" =
  local (setJobType (MPIJob 1 14) . setJobTime (4*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ 16..26 ]
  where
    nmax = 10
    bound (lMax :: Int) = Bound
      { boundKey = (ttttNoGaps nmax) { spins = fromIntegral <$> [0..lMax]}
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_binary_search_test_nmax10" =
  mapConcurrently_ search $
  [ ((oddScalar,x),(evenScalar,1/2,8)) | x <- [1,1.1..12] ] ++ [ ((evenScalar,x),(oddScalar,1/2,13)) | x <- [1,1.1..7] ]
  where
    jobType = MPIJob 1 6
    jobTime = 2*hour
    prec = 768
    nmax = 10
    search ((chanFixed,deltaFixed),(chanSearched,deltaMinSearched,deltaMaxSearched)) =
      local (setJobType jobType . setJobTime jobTime . setSlurmPartition "scavenge" . setSlurmQos "nothrottle") $
      remoteTTTT3dBinarySearch $
      TTTT3dBinarySearch
      { stress_bs_bound = Bound
        { boundKey = (ttttNoGaps nmax) { spectrum = Spectrum.setGap chanFixed deltaFixed $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum}
        , precision = prec
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , stress_bs_gapSector = chanSearched
      , stress_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = deltaMinSearched
          , falsePoint = deltaMaxSearched
          }
        , threshold  = 1e-2
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }

boundsProgram "TTTT_TTe_bound_nmax10" =
  local (setJobType (MPIJob 1 12) . setJobTime (4*hour) . setJobMemory "20G" . setSlurmPartition "scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  -- [(x,y) | x <- [0.6,0.8..6], y  <- [0.6,0.8..8]]
  [(x,y) | x <- [4,4.04..6], y  <- [2,2.02..3]]
  -- [(x,y) | x <- [4], y  <- [2]]
  where
    nmax = 10
    bound deltas = Bound
      { boundKey = ttttTTeBoundGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_TTe_ising_bound_nmax10" =
  local (setJobType (MPIJob 1 12) . setJobTime (4*hour) . setJobMemory "20G" . setSlurmPartition "pi_poland,scavenge" . setSlurmQos "nothrottle") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [((x,y),dir) | x <- [0.6,0.625..2.5], y <- [3,3.05..3.95], x < y, dir <- [UpperBound]]
  where
    nmax = 10
    bound (deltas,dir) = Bound
      { boundKey = ttttIsingTTeBoundGaps deltas dir nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_test_nmax12" =
  local (setJobType (MPIJob 19 48) . setJobTime (10*hour) . setJobMemory "0G" . setSlurmPartition "scavenge" . setSlurmConstraint "cascadelake,avx512,8268,nogpu,standard,common,bigtmp") $
  void (Bound.remoteCompute bound)
  where
    nmax = 12
    bound = Bound
      { boundKey = ttttNoGaps nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }
----------------
--- nmax=14 ----
----------------

boundsProgram "TTTT_test_nmax14" =
  local (setJobType (MPIJob 1 36) . setJobTime (12*hour) . setJobMemory "0G" . setSlurmPartition "pi_poland") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (6,7)
  ]
  where
    bound deltas = Bound
      { boundKey = (ttttFeasibleTestGaps deltas 18) { blockParams = (Params.block3dParamsNmax 18) { B3d.nmax = 14 } }
      , precision = (Params.block3dParamsNmax 18).precision
      , solverParams = (Params.jumpFindingParams 18)
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TTTT_delaunay_nmax14" =
  ttttDelaunaySearchNmax14 $
    TTTTDelaunaySearchData
    { nmax              = 14
    , affine            = AffineTransform
                            { affineShift = toV (4.25,7.25)
                            , affineLinear = toV  ( toV (3.75, 0)
                                                  , toV (0, 6.75) )
                            }
    , initialCheckpoint = Just "/vast/palmer/scratch/poland/rse23/hyperion/data/2023-05/azNfQ/Object_iTt2Cwd3tdCjomKgfNIqkwYcF6hm_6lIe08-ufUOC_c/ck"
    , initialDisallowed = toV <$> [(1, 12), (2, 10), (2, 11), (2, 12), (2, 9), (3, 9), (4, 9), (5, 9), (1/2, 12), (6, 9), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (8, 1), (8, 2), (8, 1/2)] --, (13/2, 29/4), (13/2, 13/4), (19/4, 8), (23/4, 8), (11/4, 8), (15/4, 8), (13/2, 17/4), (3/2, 43/4), (13/2, 23/4), (51/8, 37/16), (35/16, 31/4), (45/8, 15/2), (33/8, 15/2), (29/8, 15/2), (25/4, 25/4), (175/64, 119/16), (151/32, 59/8), (25/4, 31/8), (59/8, 13/16), (55/32, 623/64), (25/4, 45/8), (25/4, 35/8), (115/64, 137/16), (447/256, 4615/512), (99/16, 105/32), (923/128, 133/256), (187/32, 59/8), (163/32, 59/8), (209/32, 121/64), (99/32, 59/8), (99/16, 55/8), (14747/2048, 2053/4096), (1027/512, 977/128), (99/16, 77/32), (201/32, 33/16), (235931/32768, 32773/65536), (223/32, 73/64), (23/16, 361/32), (433/64, 201/128), (3774875/524288, 524293/1048576), (233329/32768, 49519/65536), (60397979/8388608, 8388613/16777216), (403/256, 4987/512), (391/64, 485/128), (1201/512, 953/128), (1547/1024, 20403/2048), (1675/1024, 17959/2048), (966367643/134217728, 134217733/268435456), (391/64, 201/32), (6891/4096, 68391/8192), (61/16, 15/2), (6503/4096, 74531/8192), (49/8, 65/16), (15461882267/2147483648, 2147483653/4294967296), (49/8, 191/32), (315/512, 1517/128), (49/8, 89/16), (49/8, 71/16), (783/128, 271/128), (1603/1024, 19499/2048), (247390116251/34359738368, 34359738373/68719476736), (49/8, 5), (779/128, 229/32), (2317/512, 961/128), (3958241859995/549755813888, 549755813893/1099511627776), (4437/2048, 483/64), (63331869759899/8796093022208, 8796093022213/17592186044416), (3113/512, 1753/256), (1013309916158363/140737488355328, 140737488355333/281474976710656), (1555/256, 1673/512), (195/32, 187/64)]
    , initialAllowed    = toV <$> [(1/2,11),(1,11),(1,10),(1,9),(1,8),(1,7),(2,7),(3,7),(4,7),(5,7),(6,7),(6,6),(6,5),(6,4),(6,3),(6,2),(6,1),(6,1/2)]
    , solverPrecision   = 768
    , delaunayConfig    = defaultDelaunayConfig 4 400
    , jobTime           = 6*hour
    , jobType           = MPIJob 1 36
    , jobMemory         = "0G"
    , slurmPartition    = "pi_poland"
    }

boundsProgram "TTTT_binary_search_test_nmax14" =
  mapConcurrently_ search $
  -- [ ((oddScalar,x),(evenScalar,6,8)) | x <-  [0.5,0.55..2]]
  -- [1,1.1..11.4] ] ++
  [ ((evenScalar,x),(oddScalar,1/2,3)) | x <- [6,6.05..8] ]
  where
    jobType = MPIJob 1 24
    jobTime = 1*day
    jobMemory = "30G"
    prec = (Params.block3dParamsNmax 18).precision
    nmax = 14
    search ((chanFixed,deltaFixed),(chanSearched,deltaMinSearched,deltaMaxSearched)) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition "scavenge,pi_poland" . setSlurmQos "nothrottle" . setSlurmConstraint "cascadelake") $
      remoteTTTT3dBinarySearch $
      TTTT3dBinarySearch
      { stress_bs_bound = Bound
        { boundKey = (ttttNoGaps (nmax+4))
          { spectrum = Spectrum.setGap chanFixed deltaFixed $
             Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
          , blockParams = (Params.block3dParamsNmax (nmax+4)) { B3d.nmax = nmax }}
        , precision = prec
        , solverParams = (Params.jumpFindingParams (nmax+4)) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , stress_bs_gapSector = chanSearched
      , stress_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = deltaMinSearched
          , falsePoint = deltaMaxSearched
          }
        , threshold  = 1e-2
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }

boundsProgram "TTTT_test_nmax18" =
  local (setJobType (MPIJob 20 48) . setJobTime (1*day) . setJobMemory "0G" . setSlurmPartition "scavenge") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (2/3,2/3)
  ]
  where
    nmax = 18
    bound deltas = Bound
      { boundKey = ttttFeasibleTestGaps deltas nmax
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram p = unknownProgram p
