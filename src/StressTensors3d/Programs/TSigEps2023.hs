{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PatternSynonyms     #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Programs.TSigEps2023 where

import Blocks.Blocks3d                    qualified as B3d
import Bootstrap.Bounds                   (BoundDirection (..))
import Bootstrap.Bounds.Spectrum          qualified as Spectrum
import Bootstrap.Math.AffineTransform     (AffineTransform (..), apply)
import Bootstrap.Math.DampedRational      qualified as DR
import Bootstrap.Math.Linear              (pattern V2, toM, toV)
import Bootstrap.Math.Util                (dot)
import Bootstrap.Math.VectorSpace         ((*^))
import Control.Monad.IO.Class             (liftIO)
import Control.Monad.Reader               (asks, local, void)
import Data.Aeson                        (ToJSON)
import Data.Binary                       (Binary)
import Data.Map                           qualified as Map
import Data.Matrix.Static                 (Matrix)
import Data.Proxy                         (Proxy (..))
import Data.Scientific                    (Scientific)
import Data.Scientific                    qualified as Scientific
import Data.Tagged                        (untag)
import Data.Text                          (Text)
import GHC.Generics                      (Generic)
import Hyperion
import Hyperion.Bootstrap.BinarySearch   (BinarySearchConfig (..), Bracket (..))
import Hyperion.Bootstrap.Bound           (Bound (..), CheckpointMap, LambdaMap)
import Hyperion.Bootstrap.Bound           qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch  (delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Main            (unknownProgram)
import Hyperion.Bootstrap.OPESearch       (BilinearForms (..),
                                           OPESearchConfig (..))
import Hyperion.Bootstrap.OPESearch       qualified as OPE
import Hyperion.Bootstrap.Params          qualified as Params
import Hyperion.Database                  qualified as DB
import Hyperion.Log                       qualified as Log
import Hyperion.Util                      (hour)
import Linear.V                           (V)
import SDPB qualified
import StressTensors3d.Programs.Defaults  (defaultBoundConfig,
                                           defaultDelaunayConfig,
                                           defaultQuadraticNetConfig)
import StressTensors3d.Programs.TestMixed (z2EvenParityEvenScalar,
                                           z2EvenParityOddScalar,
                                           z2OddParityEvenScalar)
import StressTensors3d.TSigEps3d          (TSigEps3d (..))
import StressTensors3d.TSigEps3d          qualified as TSigEps3d
import StressTensors3d.TSigEpsSetup       (ExternalDims (..))
import StressTensors3d.TSigEpsSetup       qualified as TSigEpsSetup
import StressTensors3d.Z2Rep              (Z2Rep (..))

z2EvenParityEvenSpin2 :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenSpin2 = TSigEpsSetup.SymmetrySector 2 B3d.ParityEven Z2Even

deltaVToExternalDims :: V 2 Rational -> ExternalDims
deltaVToExternalDims (V2 deltaSig deltaEps) =
  ExternalDims { deltaSig = deltaSig, deltaEps = deltaEps }

externalDimsToDeltaV :: ExternalDims -> V 2 Rational
externalDimsToDeltaV dims = V2 dims.deltaSig dims.deltaEps

boundDeltaV :: Bound prec TSigEps3d -> V 2 Rational
boundDeltaV bound = externalDimsToDeltaV bound.boundKey.externalDims

tSigEpsNoGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsNoGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tSigEpsFeasibleDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsFeasibleDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }


opeEllipseGuessNmax6 :: Matrix 5 5 Rational
opeEllipseGuessNmax6 = toM
  ( (50, 0, -48.136922, 0, 0)
  , (0, 10204.082, -147.3589, 0, 0)
  , (-48.136922, -147.3589, 162.7941, -72.844493, -11.289593)
  , (0, 0, -72.844493, 50, 0)
  , (0, 0, -11.289593, 0, 12.5)
  )

opeEllipseGuessNmax6' :: Matrix 5 5 Rational
opeEllipseGuessNmax6' = toM
  ( (50,0,-48.136922,0,0)
  , (0,8,-0.11552937,0,0)
  , (-48.136922,-0.11552937,150.73237,-72.844493,-0.28901358)
  , (0,0,-72.844493,50,0)
  , (0,0,-0.28901358,0,0.32)
  )


-- | This is the OPE ellipse extracted from an island computed with the above,
--  more conservative ellipse. It represents the bounding ellipse 10x bigger
-- than the space of feasible OPE vectors for all of the island's primal points.
opeEllipseEvenTNmax6 :: Matrix 5 5 Rational
opeEllipseEvenTNmax6 =
  toM ((0.359816, 0.377074, -0.277216, 0.0022255, -0.0858743)
    , (0.377074, 0.423357, -0.287684, 0.00293715, -0.0945149)
    , (-0.277216, -0.287684, 0.214176, -0.00194404, 0.065825)
    , (0.0022255, 0.00293715, -0.00194404, 0.00086518, -0.00165357)
    , (-0.0858743, -0.0945149, 0.065825, -0.00165357, 0.0227361)
    )

-- | This is the affine transform extracted in the same way as the above.
isingAffineNmax6 :: AffineTransform 2 Rational
isingAffineNmax6 =  AffineTransform
  { affineShift  = toV (0.518171,1.41269)
  , affineLinear = toV ( toV (0.000305562, 0.00349606), toV (-0.0000734378, 6.4186e-6))
  }

remoteTSigEpsOPESearch
  :: CheckpointMap TSigEps3d
  -> LambdaMap TSigEps3d (V 5 Rational)
  -> BilinearForms 5
  -> Bound Int TSigEps3d
  -> Cluster Bool
remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (static opeSearchCfg) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg =
      OPESearchConfig setLambda TSigEps3d.getMatExt (OPE.queryAllowedMixed qmConfig)
    setLambda lambda bound = bound
      { boundKey = bound.boundKey
        { TSigEps3d.objective = TSigEps3d.Feasibility (Just lambda) }
      }
    qmConfig = OPE.QueryMixedConfig
      { OPE.qmQuadraticNetConfig = defaultQuadraticNetConfig
      , OPE.qmDescentConfig      = OPE.defaultDescentConfig
      , OPE.qmResolution         = 1e-16
      , OPE.qmPrecision          = 384
      , OPE.qmHessianLineSteps   = 200
      , OPE.qmHessianLineAverage = 10
      }

data TSigEps3dBinarySearch = TSigEps3dBinarySearch
  { tse_bs_bound     :: Bound Int TSigEps3d
  , tse_bs_config    :: BinarySearchConfig Rational
  , tse_bs_dualPt :: V 2 Rational
  , tse_bs_primalPt :: V 2 Rational
  } deriving (Show, Generic, Binary, ToJSON)

remoteTSigEps3dBinarySearch :: TSigEps3dBinarySearch -> Cluster (Bracket Rational)
remoteTSigEps3dBinarySearch tse_bs = Bound.remoteBinarySearchBoundWithHotStarting Bound.MkBinarySearchBound
  { Bound.bsBoundClosure      = static mkBound `ptrAp` cPure (tse_bs.tse_bs_bound) `cAp` cPure (tse_bs.tse_bs_dualPt) `cAp` cPure (tse_bs.tse_bs_primalPt)
  , Bound.bsConfig            = tse_bs.tse_bs_config
  , Bound.bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound bound dual primal fraction =
      bound { boundKey = (boundKey bound) { externalDims = deltaVToExternalDims $ (1-fraction) *^ primal + fraction *^ dual } }
    getBool _ _ = pure <$> SDPB.isPrimalFeasible

getExtMat :: Bound Int TSigEps3d -> Bound.BoundFiles -> Job (Matrix 5 5 Scientific)
getExtMat bound' files = untag @Job $
  Bound.reifyBoundWithContext bound' $ \(bound :: Bound (Proxy p) TSigEps3d) -> do
  opeMatrix <- Bound.getBoundObject bound files TSigEps3d.getMatExt
  norm <- Bound.getBoundObject bound files $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alpha <- liftIO $ SDPB.readFunctional @(Bound.BigFloat p) norm files.outDir
  pure $
    fmap Scientific.fromFloatDigits $
    fmap (alpha `dot`) opeMatrix

logExtMatLocal :: Bound Int TSigEps3d -> Bound.BoundFiles -> Cluster ()
logExtMatLocal bound files = do
  progInfo <- asks clusterProgramInfo
  extMat <- liftIO $ runJobLocal progInfo $ getExtMat bound files
  Log.info "External matrix" extMat

coerceBase :: DR.DampedRational base1 f a -> DR.DampedRational base2 f a
coerceBase DR.DampedRational{..} = DR.DampedRational{..}

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_test_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (6*hour) . setJobMemory "0G") $
  --local (setJobType (MPIJob 1 127) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "shared") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (toV (0.5181489, 1.412625),           Just lambda)
  --, (toV (0.518144590625, 1.41266015625), Just lambda)
  ]
  where
    lambda = toV
      ( (-38757737) / 88786972
      , (-82533) / 13425418
      , (-111357690) / 245817499
      , (-64848956) / 98255647
      , (-54187949) / 131974577
      )
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TSigEps_OPEScan_test_nmax6" =
  local (setJobType (MPIJob 1 127) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

-- | Note: running this with 64 threads and 128G of memory ran into OOM issues
boundsProgram "TSigEps_Island_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (6*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    nmax = 6
    paramNmax = 6
    affine = isingAffineNmax6
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = apply affine <$> [toV (x,y) | x <- [-1,1], y <- [-1,1]]
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "TSigEps_ExtOPEBound_nmax6" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" ) $
  mapConcurrently_ (Bound.remoteComputeKeepFiles . bound)
  [ (toV (0.5181489, 1.412625),  toV ( (-39353909) / 90158408
                            , (-350635) / 56507009
                            , (-136589237) / 301491712
                            , (-104165758) / 157848095
                            , (-51410107) / 125167736 ))
  ]
  where
    nmax = 6
    paramNmax = 8
    -- precision = 2048
    bound (dExt, lambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt (Just lambda) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        , objective    = TSigEps3d.ExternalOPEBound lambda UpperBound
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024, SDPB.writeSolution = SDPB.allSolutionParts }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_test_ExternalOPEFormBound_nmax6_files_for_Petr" =
  local (setJobType (MPIJob 1 32) . setJobTime (6*hour)) $
  doConcurrently_ $ do
  lambdaForm <- lambdaForms
  let b = bound lambdaForm
  pure $ do
    (_,files) <- Bound.remoteComputeKeepFiles b
    logExtMatLocal b files
  where
    nmax = 6
    deltaExt = toV (0.5181489, 1.412625)
    lambdaForms =
      [ toM
        ( ( 0, 0, 1, 0, 0)
        , ( 0, 0, 0, 0, 0)
        , ( 1, 0, 0, 0, 0)
        , ( 0, 0, 0, 0, 0)
        , ( 0, 0, 0, 0, 0)
        )
      ]
    bound lambdaForm = Bound
      { boundKey = TSigEps3d
        { spectrum     = Spectrum.setGaps
          [ (z2EvenParityEvenScalar, 3 )
          , (z2OddParityEvenScalar, 3 )
          , (z2EvenParityOddScalar, 3 )
          ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
        , objective    = TSigEps3d.ExternalOPEFormBound lambdaForm
        , blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = map fromIntegral $ Params.spinsNmax 12
        , externalDims = deltaVToExternalDims deltaExt
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_grid_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ toV (5181966689/10000000000, 14126542907/10000000000)
    , toV (41451765811/80000000000, 113010682093/80000000000)
    , toV (13267002881/25600000000, 180948376313/128000000000)
    , toV (10609713092177/20480000000000, 28906067058241/20480000000000)
    , toV (21222570145561/40960000000000, 57833842903433/40960000000000)
    , toV (530685745021/1024000000000, 1447619160393/1024000000000)
    , toV (21228365841927/40960000000000, 57904604103991/40960000000000)
    , toV (169812973654061/327680000000000, 463033775321053/327680000000000)
    , toV (1358568682526427/2621440000000000, 3705127289264811/2621440000000000)
    , toV (339538024043827/655360000000000, 925104881197891/655360000000000)
    , toV (169824674275653/327680000000000, 463279921698609/327680000000000)
    , toV (339602187057741/655360000000000, 925778037760893/655360000000000)
    , toV (135858502394443/262144000000000, 370540672496667/262144000000000)
    , toV (339652346194739/655360000000000, 926576487178547/655360000000000)
    , toV (84913457184483/163840000000000, 231639617077539/163840000000000)
    , toV (67915400618423/131072000000000, 185098210918983/131072000000000)
    , toV (1358383459400409/2621440000000000, 3702834299714377/2621440000000000)
    , toV (5434135901285043/10485760000000000, 14818705405476099/10485760000000000)
    , toV (169827020817161/327680000000000, 463273404357833/327680000000000)
    , toV (271686430145519/524288000000000, 29626978635863/20971520000000)
    , toV (1358310919047503/2621440000000000, 3702053474451039/2621440000000000)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 8
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_peninsula_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (x,y) | x <- [0.8,0.9..1.4], y <- [1.8,1.9..3]
  ]
  where
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt Nothing paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 960, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_continent_binary_nmax6" =
  mapConcurrently_ search $
  [ (dualPt, toV (1,2)) | dualPt <- dualPts ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False } 
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }
    lowerPts = [toV (x,1) | x <- [0.7,0.75..1]]
    leftPts = [toV (0.7,y) | y <- [1.025,1.05..2.975]]
    upperPts = [toV (x,3) | x <- [0.7,0.75..1]]
    dualPts = lowerPts ++ leftPts ++ upperPts

boundsProgram "TSigEps_continent_kink_nmax6" =
  mapConcurrently_ search $
  [ (toV (x,2.5), toV (x,1.75)) | x <- [0.8,0.81..1] ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False } 
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }


---------------
--- nmax=10 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "large-shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }


---------------
--- nmax=14 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax14" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ((-54537050) / 124926441,(-242728) / 39635427,(-72625868) / 160316979,(-43886811) / 66495941,(-48814271) / 118892408)
    affine = isingAffineNmax6
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
      , boundConfig = defaultBoundConfig
      }

boundsProgram p = unknownProgram p

initialNmax6DisallowedPts :: [V 2 Rational]
initialNmax6DisallowedPts = toV <$> [(2053/4000, 1079/800), (64/125, 269/200), (2053/4000, 1083/800), (64/125, 67/50), (64/125, 34/25), (64/125, 27/20), (64/125, 271/200), (2053/4000, 1087/800), (64/125, 273/200), (2053/4000, 1091/800), (64/125, 137/100), (2053/4000, 219/160), (2053/4000, 1103/800), (2053/4000, 1099/800), (64/125, 11/8), (64/125, 69/50), (2053/4000, 1107/800), (2053/4000, 1111/800), (1029/2000, 543/400), (1029/2000, 109/80), (1029/2000, 547/400), (1029/2000, 549/400), (1029/2000, 551/400), (1029/2000, 553/400), (1029/2000, 557/400), (1029/2000, 111/80), (1029/2000, 559/400), (2063/4000, 1093/800), (2063/4000, 1097/800), (2063/4000, 1101/800), (2063/4000, 221/160), (2063/4000, 1113/800), (2063/4000, 1109/800), (2063/4000, 1117/800), (2063/4000, 1121/800), (517/1000, 11/8), (2063/4000, 45/32), (517/1000, 69/50), (517/1000, 277/200), (517/1000, 279/200), (517/1000, 7/5), (517/1000, 141/100), (517/1000, 281/200), (517/1000, 283/200), (2073/4000, 1111/800), (2073/4000, 223/160), (2073/4000, 1107/800), (2073/4000, 1119/800), (2073/4000, 1123/800), (2073/4000, 1127/800), (2073/4000, 1131/800), (2073/4000, 227/160), (1039/2000, 557/400), (2073/4000, 1139/800), (1039/2000, 559/400), (1039/2000, 561/400), (1039/2000, 563/400), (1039/2000, 113/80), (1039/2000, 569/400), (1039/2000, 567/400), (1039/2000, 571/400), (1039/2000, 573/400), (2083/4000, 1121/800), (2083/4000, 45/32), (2083/4000, 1129/800), (2083/4000, 1133/800), (2083/4000, 1137/800), (2083/4000, 1141/800), (2083/4000, 229/160), (2083/4000, 1149/800), (2083/4000, 1153/800), (261/500, 141/100), (261/500, 283/200), (261/500, 71/50), (261/500, 57/40), (261/500, 143/100), (261/500, 287/200), (261/500, 36/25), (261/500, 289/200), (261/500, 29/20), (517/1000, 139/100)]

moreNmax6DisallowedPts :: [V 2 Rational]
moreNmax6DisallowedPts = toV <$> [(64/125, 67/50), (64/125, 269/200), (64/125, 27/20), (64/125, 271/200), (64/125, 34/25), (64/125, 273/200), (64/125, 137/100), (64/125, 69/50), (64/125, 11/8), (2053/4000, 1079/800), (2053/4000, 1083/800), (2053/4000, 1087/800), (2053/4000, 219/160), (2053/4000, 1091/800), (2053/4000, 1099/800), (2053/4000, 1103/800), (2053/4000, 1107/800), (2053/4000, 1111/800), (1029/2000, 543/400), (1029/2000, 109/80), (1029/2000, 547/400), (1029/2000, 549/400), (1029/2000, 551/400), (1029/2000, 553/400), (1029/2000, 111/80), (1029/2000, 557/400), (1029/2000, 559/400), (2063/4000, 1093/800), (2063/4000, 1097/800), (2063/4000, 1101/800), (2063/4000, 221/160), (2063/4000, 1109/800), (2063/4000, 1113/800), (2063/4000, 1117/800), (2063/4000, 1121/800), (517/1000, 11/8), (2063/4000, 45/32), (517/1000, 69/50), (517/1000, 277/200), (517/1000, 139/100), (517/1000, 279/200), (517/1000, 7/5), (517/1000, 141/100), (517/1000, 281/200), (517/1000, 283/200), (2073/4000, 1107/800), (2073/4000, 223/160), (2073/4000, 1111/800), (2073/4000, 1119/800), (2073/4000, 1123/800), (2073/4000, 1127/800), (2073/4000, 1131/800), (2073/4000, 227/160), (1039/2000, 557/400), (2073/4000, 1139/800), (1039/2000, 559/400), (1039/2000, 561/400), (1039/2000, 563/400), (1039/2000, 567/400), (1039/2000, 113/80), (1039/2000, 569/400), (1039/2000, 571/400), (1039/2000, 573/400), (2083/4000, 1121/800), (2083/4000, 45/32), (2083/4000, 1129/800), (2083/4000, 1133/800), (2083/4000, 1137/800), (2083/4000, 229/160), (2083/4000, 1141/800), (2083/4000, 1149/800), (2083/4000, 1153/800), (261/500, 141/100), (261/500, 283/200), (261/500, 71/50), (261/500, 57/40), (261/500, 143/100), (261/500, 287/200), (261/500, 289/200), (261/500, 36/25), (261/500, 29/20), (10351489/20000000, 22521/16000), (10370239/20000000, 11313/8000), (8288489/16000000, 18069/12800), (165806269/320000000, 361641/256000), (82881673/160000000, 90251/64000), (16594239/32000000, 18121/12800), (663562667/1280000000, 724229/512000), (663172043/1280000000, 1446117/1024000), (663169469/1280000000, 361339/256000), (1326918441/2560000000, 1447847/1024000), (2652635139/5120000000, 5784021/4096000), (331686297/640000000, 2894107/2048000), (1326383333/2560000000, 2892477/2048000), (10610937709/20480000000, 23138511/16384000), (5307180109/10240000000, 11578601/8192000), (1326498543/2560000000, 11568923/8192000)]
