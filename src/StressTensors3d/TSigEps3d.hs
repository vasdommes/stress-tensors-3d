{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TSigEps3d
  ( TSigEps3d(..)
  , Objective(..)
  , getMatExt
  , writeCrossingEqsTSigEps3d
  , writeOPECoeffsTSigEps3d
  ) where

import Data.Foldable (foldl')
import Blocks                          (BlockFetchContext, Coordinate (XT),
                                        CrossingMat, Delta (..),
                                        Derivative (..), TaylorCoeff (..),
                                        Taylors)
import Blocks.Blocks3d                 (ConformalRep (..), Parity (..))
import Blocks.Blocks3d                 qualified as B3d
import Bootstrap.Bounds                (BoundDirection, FourPointFunctionTerm,
                                        OPECoefficient, OPECoefficientExternal,
                                        Spectrum, addOPECoeffExt, boundDirSign,
                                        opeCoeffExternalSimple, opeCoeffIdentical_,
                                        opeCoeffGeneric_, scaleOPECoeff, addOPECoeff)
import Bootstrap.Bounds                qualified as Bounds
import Bootstrap.Math.FreeVect         (FreeVect, (*^))
import Bootstrap.Math.HalfInteger      (HalfInteger)
import Bootstrap.Math.Linear           (bilinearPair, toV)
import Bootstrap.Math.Linear           qualified as L
import Bootstrap.Math.VectorSpace      (zero)
import Data.Aeson                      (FromJSON, ToJSON)
import Data.Binary                     (Binary)
import Data.Matrix.Static              (Matrix)
import Data.Maybe                      (maybeToList)
import Data.Reflection                 (Reifies, reflect)
import Data.Proxy (Proxy(..))
import Data.Tagged                     (Tagged, untag)
import Data.Text                       qualified as Text
import Data.Vector                     (Vector)
import Data.Vector                     qualified as Vector
import GHC.Generics                    (Generic)
import GHC.TypeNats                    (KnownNat)
import Hyperion                        (Dict (..), Static (..))
import Hyperion.Bootstrap.Bound        (BuildInJob, SDPFetchBuildConfig (..),
                                        ToSDP (..), reifyNatFromInt)
import Linear.V                        (V)
import SDPB qualified
import StressTensors3d.ConstantBlock   (appendConstantMatrix)
import StressTensors3d.CrossingEqs     (crossingEqsStructSet, structsSSSS,
                                        structsTSSS, structsTTSS, structsTTTS,
                                        structsTTTT)
import StressTensors3d.CrossingEqUtils (writeCrossingEqs)
import StressTensors3d.OPECoeffUtils   (writeOPECoeffs)
import StressTensors3d.TSig3d          (TSig3d, tsBuildChain, tsFetchConfig)
import StressTensors3d.TSigEpsSetup    (ExternalDims (..), ExternalOp (..),
                                        SymmetrySector (..), TSMixedBlock,
                                        TSMixedStruct, lambdaTTEps, lambda_B,
                                        lambda_F, listSpectrum, opeSS,
                                        opeSigEps, opeTS, opeTT, so3,
                                        toTSMixedBlock, wardSST)
import StressTensors3d.TTTT3d          (withNonEmptyVec)
import StressTensors3d.Z2Rep           (Z2Rep (..))
import Type.Reflection                 (Typeable)


data TSigEps3d = TSigEps3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 5 Rational))
  | ExternalOPEBound (V 5 Rational) BoundDirection
  | ExternalOPEFormBound (Matrix 5 5 Rational)
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type TSigEpsNumCrossing = 53

-- | Crossing equations for the T-Sig-Eps system
crossingEqs
  :: forall s b a . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TSigEpsNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,T,Eps),       structsTTTS)
  , ((T,T,Sig,Sig),     structsTTSS)
  , ((Sig,T,Sig,T),     structsTTSS)
  , ((T,T,Eps,Eps),     structsTTSS)
  , ((Eps,T,Eps,T),     structsTTSS)
  , ((Eps,T,Eps,Eps),   structsTSSS)
  , ((Eps,T,Sig,Sig),   structsTSSS)
  , ((Sig,T,Sig,Eps),   structsTSSS)
  , ((Sig,Sig,Sig,Sig), structsSSSS)
  , ((Eps,Eps,Eps,Eps), structsSSSS)
  , ((Sig,Sig,Eps,Eps), structsSSSS)
  , ((Sig,Eps,Sig,Eps), structsSSSS)
  ]

-- | Write the crossing equations to a file in TeX format
writeCrossingEqsTSigEps3d :: FilePath -> IO ()
writeCrossingEqsTSigEps3d path = writeCrossingEqs path (crossingEqs @())

-- | Write the OPE coefficients to a file in TeX format
writeOPECoeffsTSigEps3d :: FilePath -> TSigEps3d -> IO ()
writeOPECoeffsTSigEps3d path t = writeOPECoeffs path ope opeExt t

derivsVecTSigEps3d :: TSigEps3d -> V TSigEpsNumCrossing (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecTSigEps3d f = Bounds.derivsVec crossingEqs <*> pure f.blockParams.nmax

crossingMatTSigEps3d
  :: forall s j a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) TSMixedStruct a)
  -> Tagged s (CrossingMat j TSigEpsNumCrossing TSMixedBlock  a)
crossingMatTSigEps3d channel =
  pure $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrix channel (crossingEqs @s)

ope
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> concat
    [ opeTT r parity
    , opeSS Sig r parity
    , opeSS Eps r parity
    , opeTS Eps r parity
    ]
  Z2Odd  -> opeTS Sig r parity ++ opeSigEps r parity

gffOpe
  :: forall a s. (Eq a, Floating a, Reifies s ExternalDims)
  => V 1 (OPECoefficient (ExternalOp s) TSMixedStruct a)
gffOpe = toV $
  foldl' addOPECoeff zeroOpe $
  map (uncurry scaleOPECoeff) opeData
  where
    dSig = (reflect @s Proxy).deltaSig
    dEps = (reflect @s Proxy).deltaEps
    zeroOpe _ _ = 0
    -- TODO: Put sig d d sig, eps d d eps, and T d box^n T here
    opeData =
      [ (sqrt 2, opeCoeffIdentical_ Sig (ConformalRep (2*dSig) 0) (so3 0 0))
      , (sqrt 2, opeCoeffIdentical_ Eps (ConformalRep (2*dEps) 0) (so3 0 0))
      , (1,      opeCoeffGeneric_ Sig Eps (ConformalRep (dSig+dEps) 0) (so3 0 0) (so3 0 0))
      ]

constraintLabel :: Z2Rep -> Parity -> HalfInteger -> Text.Text
constraintLabel z2 parity j =
  Text.pack $ show z2 <> "_" <> show parity <> "_" <> show j

bulkConstraints
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext TSMixedBlock a m, Reifies s ExternalDims)
  => TSigEps3d
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints t = pure $ do
  (delta, range, SymmetrySector j parity z2) <- listSpectrum t.spins t.spectrum
  let
    opeCoeffs = ope (ConformalRep delta j) parity z2
    params = (t.blockParams, (t.blockParams, t.blockParams))
    label = constraintLabel z2 parity j
  maybeToList $ withNonEmptyVec opeCoeffs $
    Bounds.bootstrapConstraintLabeled label params (derivsVecTSigEps3d t) range .
    untag @s .
    crossingMatTSigEps3d

-- | See the Note in 'StressTensors3d.TSigEpsSetup' about how we handle
-- Ward identities. The external OPE coefficients are:
--
-- (<TTT>_B, <TTT>_F, <SSE>, <EEE>, <TTE>)
opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s ExternalDims)
  => V 5 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( lambda_B `addOPECoeffExt` wardSST Sig `addOPECoeffExt` wardSST Eps
  , lambda_F `addOPECoeffExt` wardSST Sig `addOPECoeffExt` wardSST Eps
  , opeCoeffExternalSimple Sig Sig Eps (so3 0 0)
  , opeCoeffExternalSimple Eps Eps Eps (so3 0 0)
  , lambdaTTEps
  )

matExt
  :: forall a s . (Fractional a, Eq a,  Reifies s ExternalDims)
  => Tagged s (CrossingMat 5 TSigEpsNumCrossing TSMixedBlock a)
matExt =
  pure $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Sig,Eps]

getMatExt
  :: (BlockFetchContext TSMixedBlock a m, Applicative m, RealFloat a)
  => TSigEps3d
  -> m (Matrix 5 5 (Vector a))
getMatExt t =
  Bounds.getIsolatedMat params (derivsVecTSigEps3d t) (Bounds.runTagged t.externalDims matExt)
  where
    params = (t.blockParams, (t.blockParams, t.blockParams))

matUnit
  :: forall s a . (Reifies s ExternalDims , Fractional a, Eq a)
  => Tagged s (CrossingMat 1 TSigEpsNumCrossing B3d.IdentityBlock3d a)
matUnit = pure $ B3d.identityCrossingMat (crossingEqs @s)

tSigEps3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m, BlockFetchContext TSMixedBlock a m)
  => TSigEps3d
  -> SDPB.SDP m a
tSigEps3dSDP t = Bounds.runTagged t.externalDims $ do
  bulk <- bulkConstraints t
  unit <- matUnit
  ext <- matExt
  -- TODO: use GFF!
  -- gff <- crossingMatTSigEps3d gffOpe
  let
    dv = derivsVecTSigEps3d t
    params = (t.blockParams, (t.blockParams, t.blockParams))
  (cons, obj, norm) <- case t.objective of
    Feasibility mLambda -> pure $
      let
        extConstraint = case mLambda of
          Just lambda -> Bounds.isolatedConstraintLabeled "ext" params dv $
            bilinearPair (fmap fromRational lambda) ext
          Nothing -> Bounds.isolatedConstraintLabeled "ext" params dv ext
      in
        ( bulk ++ [extConstraint]
        , Bounds.bootstrapObjective t.blockParams dv $ zero `asTypeOf` unit
        , Bounds.bootstrapNormalization t.blockParams dv unit
        )
    ExternalOPEBound lambda direction -> pure $
      let
        matLambda = boundDirSign direction *^ bilinearPair (fmap fromRational lambda) ext
      in
        ( bulk
        , Bounds.bootstrapObjective t.blockParams dv unit
        , Bounds.bootstrapNormalization params dv matLambda
        )
    ExternalOPEFormBound m -> do
      let
        dv' = dv L.++ toV (Vector.singleton (TaylorCoeff (Derivative (0,0))))
        extConstraint' = Bounds.isolatedConstraintLabeled "ext" (params, ()) dv' $
          appendConstantMatrix (fmap (negate . fromRational) m) ext
      bulk' <- sequence $ do
        (delta, range, SymmetrySector j parity z2) <- listSpectrum t.spins t.spectrum
        let opeCoeffs = ope (ConformalRep delta j) parity z2
        maybeToList $ withNonEmptyVec opeCoeffs $ \coeffs ->
          Bounds.bootstrapConstraintLabeled (constraintLabel z2 parity j) (params, ()) dv' range .
          appendConstantMatrix zero <$>
          crossingMatTSigEps3d coeffs
      pure $
        ( bulk' ++ [extConstraint']
        , Bounds.bootstrapObjective (t.blockParams, ()) dv' $
          appendConstantMatrix zero unit
        , Bounds.bootstrapNormalization (t.blockParams, ()) dv' $
          appendConstantMatrix (pure 1) (zero `asTypeOf` unit)
        )
  pure $ SDPB.SDP obj norm cons

instance ToSDP TSigEps3d where
  type SDPFetchKeys TSigEps3d = SDPFetchKeys TSig3d
  toSDP = tSigEps3dSDP
  reifyPrecisionWithFetchContext _ _ n go = reifyNatFromInt n go

instance SDPFetchBuildConfig TSigEps3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TSigEps3d)              where closureDict = static Dict
instance Static (Show TSigEps3d)                where closureDict = static Dict
instance Static (ToSDP TSigEps3d)               where closureDict = static Dict
instance Static (ToJSON TSigEps3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TSigEps3d) where closureDict = static Dict
instance Static (BuildInJob TSigEps3d)          where closureDict = static Dict
