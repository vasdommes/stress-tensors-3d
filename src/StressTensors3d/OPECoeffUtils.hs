{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

-- | This module defines 'writeOPECoeffs' for writing OPE coefficients
-- to files for various subsets of the T-Sig-Eps system.

module StressTensors3d.OPECoeffUtils
  ( writeOPECoeffs
  ) where

import Blocks                       (Delta)
import Blocks.Blocks3d              (ConformalRep (..), Parity (..))
import Bootstrap.Bounds             (DeltaRange (..), OPECoefficient,
                                     OPECoefficientExternal, Spectrum)
import Bootstrap.Bounds             qualified as Bounds
import Bootstrap.Math.FreeVect      (FreeVect)
import Bootstrap.Math.HalfInteger   (HalfInteger)
import Control.Monad                (guard)
import Data.Foldable                qualified as Foldable
import Data.Kind                    (Type)
import Data.List.Extra              (enumerate)
import Data.Map.Strict              (Map)
import Data.Map.Strict              qualified as Map
import Data.Proxy                   (Proxy (..))
import Data.Reflection              (Reifies, reify)
import Data.Text                    (Text)
import Data.Text                    qualified as Text
import Data.Text.IO                 qualified as Text
import GHC.Records                  (HasField (..))
import Linear.V                     (V)
import StressTensors3d.ToTeX        (ToTeX (..))
import StressTensors3d.TSigEpsSetup (ExternalDims (..), ExternalOp (..),
                                     SymmetrySector (..), TSMixedStruct)
import StressTensors3d.Z2Rep        (Z2Rep (..))

opeCoeffToMap
  :: (Enum o, Bounded o, Ord o, Ord s, Num a, Eq a)
  => OPECoefficient o s a
  -> Map (o,o) (FreeVect s a)
opeCoeffToMap coeff = Map.fromList $ do
  o1 <- enumerate
  o2 <- enumerate
  let c = coeff o1 o2
  guard $ c /= 0
  pure ((o1,o2), c)

opeCoeffExternalToMap
  :: (Enum o, Bounded o, Ord o, Ord s, Num a, Eq a)
  => OPECoefficientExternal o s a
  -> Map (o,o,o) (FreeVect s a)
opeCoeffExternalToMap coeff = Map.fromList $ do
  o1 <- enumerate
  o2 <- enumerate
  o3 <- enumerate
  let c = coeff o1 o2 o3
  guard $ c /= 0
  pure ((o1,o2,o3), c)

data OPEVector v a = OPEVector (v a)

instance (Foldable v, ToTeX a) => ToTeX (OPEVector v a) where
  toTeX (OPEVector xs) = mconcat
    [ "\\begin{align*}"
    , "\\begin{array}{|lll|}\\hline"
    , Text.intercalate "\\\\ \n\\hline\n" $
      map toTeX $ Foldable.toList xs
    , "\\\\ \\hline\\end{array}"
    , "\\end{align*}"
    ]

opeCoeffsToText
  :: (Enum o, Bounded o, Ord o, ToTeX o, Ord s, ToTeX s
     , Ord a, Num a, ToTeX a, Functor v, Foldable v)
  => v (OPECoefficient o s a)
  -> Text
opeCoeffsToText = toTeX . OPEVector . fmap opeCoeffToMap

opeCoeffsExternalToText
  :: (Enum o, Bounded o, Ord o, ToTeX o, Ord s, ToTeX s
     , Ord a, Num a, ToTeX a, Functor v, Foldable v)
  => v (OPECoefficientExternal o s a)
  -> Text
opeCoeffsExternalToText = toTeX . OPEVector . fmap opeCoeffExternalToMap

type OPEFunction =
  forall a (s :: Type) . (Fractional a, Eq a, Reifies s ExternalDims)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]

opeCoeffsToTextTEps3d
  :: ( HasField "externalDims" b ExternalDims
     , HasField "spectrum"     b (Spectrum SymmetrySector)
     , HasField "spins"        b [HalfInteger]
     )
  => OPEFunction
  -> b
  -> Text
opeCoeffsToTextTEps3d mkOPE t =
  reify t.externalDims $ \(_ :: Proxy s) ->
  mconcat $ do
  z2 <- [Z2Even,Z2Odd]
  parity <- [ParityEven, ParityOdd]
  j <- t.spins
  (delta, range) <- Bounds.listDeltas (SymmetrySector j parity z2) t.spectrum
  let
    opeCoeffs = mkOPE @Double @s (ConformalRep delta j) parity z2
    rangeChar = case range of
      Isolated  -> "="
      Continuum -> "\\geq"
  guard $ not $ null opeCoeffs
  pure $ mconcat
    [ "\\subsection*{"
    , Text.pack (show z2), ", "
    , Text.pack (show parity), ", "
    , "$j = ", toTeX j, "$, "
    , "$\\Delta ", rangeChar, " ", toTeX delta, "$}\n"
    , opeCoeffsToText opeCoeffs
    , "\n\n"
    ]

type OPEExtFunction n =
  forall a (s :: Type) . (Eq a, Fractional a, Reifies s ExternalDims)
  => V n (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)

opeCoeffsExternalToTextTEps3d
  :: (HasField "externalDims" b ExternalDims)
  => OPEExtFunction n
  -> b
  -> Text
opeCoeffsExternalToTextTEps3d mkOPEExt t =
  reify t.externalDims $ \(_ :: Proxy s) ->
  opeCoeffsExternalToText $ mkOPEExt @Double @s

writeOPECoeffs
  :: ( HasField "externalDims" b ExternalDims
     , HasField "spectrum"     b (Spectrum SymmetrySector)
     , HasField "spins"        b [HalfInteger]
     )
  => FilePath
  -> OPEFunction
  -> OPEExtFunction n
  -> b
  -> IO ()
writeOPECoeffs path ope opeExt t = Text.writeFile path $ mconcat
  [ "\\subsection*{External operators}"
  , opeCoeffsExternalToTextTEps3d opeExt t
  , opeCoeffsToTextTEps3d ope t
  ]

