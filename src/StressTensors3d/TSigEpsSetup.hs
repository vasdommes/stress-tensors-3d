{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DefaultSignatures    #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DerivingStrategies   #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE MultiWayIf           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TSigEpsSetup
  ( ExternalOp(..)
  , TSMixedBlock
  , TSMixedStruct
  , TSMixedStructLabel(..)
  , ExternalDims(..)
  , SymmetrySector(..)
  , listSpectrum
  , so3
  , opeSS
  , opeSigEps
  , opeTS
  , opeTT
  , wardSST
  , lambda_B
  , lambda_F
  , lambdaTTEps
  , toTSMixedBlock
  ) where

import Blocks                         (Block (..), BlockBase, BlockBaseGeneric,
                                       BlockFetchContext,
                                       BlockFetchContextGeneric,
                                       BlockTableParams,
                                       BlockTableParamsGeneric, ContinuumBlock,
                                       Delta (..), Derivative (..),
                                       IsolatedBlock, KnownCoordinate)
import Blocks.Blocks3d                (ConformalRep (..), Parity (..),
                                       SO3StructLabel (..))
import Blocks.Blocks3d                qualified as B3d
import Bootstrap.Bounds               (DeltaRange, HasRep (..), OPECoefficient,
                                       OPECoefficientExternal, Spectrum,
                                       listDeltas, makeStructure, mapOPEExtOps,
                                       mapOPEOps, opeCoeffGeneric_,
                                       opeCoeffIdentical_, scaleOPECoeffExt)
import Bootstrap.Math.FreeVect        (FreeVect, mapBasis, vec, (*^))
import Bootstrap.Math.HalfInteger     (HalfInteger, pattern AnInteger,
                                       pattern EvenInteger)
import Bootstrap.Math.Linear          (fromV)
import Data.Aeson                     (FromJSON, FromJSONKey, ToJSON, ToJSONKey)
import Data.Binary                    (Binary)
import Data.List.Extra                (enumerate)
import Data.Reflection                (Reifies, reflect)
import GHC.Generics                   (Generic)
import Hyperion                       (Dict (..), Static (..))
import Hyperion.Bootstrap.Util        (ToPath (..), dirScatteredHashBasePath)
import StressTensors3d.CompositeBlock (CompositeBlock (..), CompositeBlockKey,
                                       ToSO3Structs, threePtSO3_to_B3dSO3)
import StressTensors3d.CrossingEqs    (HasT (..))
import StressTensors3d.ThreePtStruct  (ThreePtStruct (..), mapOPEExtStructLabel,
                                       mapOPEStructLabel)
import StressTensors3d.ToTeX          (ToTeX (..))
import StressTensors3d.TSOStructLabel (TSOStructLabel (..))
import StressTensors3d.TTOStructLabel (TTOStructLabel (..))
import StressTensors3d.TTTT3d         (TTTTBlock)
import StressTensors3d.TTTT3d         qualified as TTTT3d
import StressTensors3d.Z2Rep          (Z2Rep (..))
import System.FilePath.Posix          ((<.>), (</>))

data ExternalOp s = T | Sig | Eps
  deriving (Show, Eq, Ord, Enum, Bounded)

instance ToTeX (ExternalOp s) where
  toTeX Sig = "\\sigma"
  toTeX Eps = "\\epsilon"
  toTeX T   = "T"

instance HasT (ExternalOp s) where
  stressTensor = T

data ExternalDims = ExternalDims
  { deltaSig :: Rational
  , deltaEps :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (ConformalRep Rational) where
  rep T = ConformalRep 3 2
  rep x = case x of
    Sig -> ConformalRep dims.deltaSig 0
    Eps -> ConformalRep dims.deltaEps 0
    where
      dims = reflect x

data SymmetrySector = SymmetrySector HalfInteger Parity Z2Rep
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

listSpectrum
  :: [HalfInteger]
  -> Spectrum SymmetrySector
  -> [(Delta, DeltaRange, SymmetrySector)]
listSpectrum spins spectrum = do
  z2     <- enumerate
  parity <- enumerate
  j      <- spins
  let symSector = SymmetrySector j parity z2
  (delta, range) <- listDeltas symSector spectrum
  pure (delta, range, symSector)

data TSMixedStructLabel
  = SO3Label SO3StructLabel
  | TSOLabel TSOStructLabel
  | TTOLabel TTOStructLabel
  deriving (Eq, Ord, Show, Generic, Binary, ToSO3Structs)

instance ToTeX TSMixedStructLabel where
  toTeX (SO3Label l) = toTeX l
  toTeX (TSOLabel l) = toTeX l
  toTeX (TTOLabel l) = toTeX l

instance ToPath (CompositeBlockKey TSMixedStructLabel) where
  toPath dir k = dir </> dirScatteredHashBasePath "t_s_mixed_block" k <.> "dat"
instance Static (ToPath (CompositeBlockKey TSMixedStructLabel)) where
  closureDict = static Dict

instance Static (Binary       TSMixedStructLabel) where closureDict = static Dict
instance Static (Show         TSMixedStructLabel) where closureDict = static Dict
instance Static (ToSO3Structs TSMixedStructLabel) where closureDict = static Dict

type TSMixedStruct = ThreePtStruct Delta TSMixedStructLabel

-- | We distinguish between three types of blocks:
data TSMixedBlock
  = PureBlock3d B3d.Block3d
  -- ^ a "pure" Block3d. Stored in Job-specific location.
  | PureTTTTBlock TTTTBlock
  -- ^ a TTTTBlock. Stored in a central location.
  | MixedBlock (CompositeBlock TSMixedStructLabel)
  -- ^ a block with a possible mixture of TTO, TSO, and SSO
  -- structures. Stored in a Job-specific location.
    deriving (Eq, Ord, Show, Binary, Generic)

type instance BlockFetchContext TSMixedBlock a m = BlockFetchContextGeneric TSMixedBlock a m
type instance BlockTableParams  TSMixedBlock     = BlockTableParamsGeneric  TSMixedBlock
type instance BlockBase         TSMixedBlock a   = BlockBaseGeneric         TSMixedBlock a

instance ToTeX TSMixedBlock where
  toTeX (PureBlock3d b)   = toTeX b
  toTeX (PureTTTTBlock b) = toTeX b
  toTeX (MixedBlock b)    = toTeX b

instance (KnownCoordinate c, RealFloat a) => IsolatedBlock  TSMixedBlock (Derivative c) a
instance (KnownCoordinate c, RealFloat a) => ContinuumBlock TSMixedBlock (Derivative c) a

toTSMixedBlock :: Block (ThreePtStruct Delta TSMixedStructLabel) B3d.Q4Struct -> TSMixedBlock
toTSMixedBlock b = case (b.struct12.label, b.struct43.label) of
  (SO3Label l12, SO3Label l43) -> PureBlock3d $ B3d.Block3d $
    b { struct12 = threePtSO3_to_B3dSO3 $ b.struct12 { label = l12 }
      , struct43 = threePtSO3_to_B3dSO3 $ b.struct43 { label = l43 }
      }
  (TTOLabel l12, TTOLabel l43) -> PureTTTTBlock $ CompositeBlock $
    b { struct12 = b.struct12 { label = l12 }
      , struct43 = b.struct43 { label = l43}
      }
  _ -> MixedBlock $ CompositeBlock b

so3 :: (Fractional a, Eq a) => HalfInteger -> HalfInteger -> FreeVect TSMixedStructLabel a
so3 j12 j123 = vec (SO3Label (SO3StructLabel j12 j123))

-- | OPE coefficients for <phi phi O>, where phi is Sig or Eps
opeSS
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ExternalOp s
  -> ConformalRep Delta
  -> Parity
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
opeSS phi r@(ConformalRep _ j) parity =
  map (opeCoeffIdentical_ phi r) $
  case (parity, j) of
    (ParityEven, EvenInteger @Int _) -> [so3 0 j]
    _                                -> []

-- | OPE coefficients for <Sig Eps O>
opeSigEps
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ConformalRep Delta
  -> Parity
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
opeSigEps r@(ConformalRep _ j) parity = case (parity, j) of
  (ParityEven, AnInteger @Int l) ->
    [opeCoeffGeneric_ Sig Eps r (so3 0 j) ((-1)^l *^ so3 0 j)]
  _ -> []

-- | OPE coefficients for <T phi O>, where phi is Sig or Eps
opeTS
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ExternalOp s
  -> ConformalRep Delta
  -> Parity
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
opeTS phi r@(ConformalRep _ j) parity =
  map (mapOPEStructLabel TSOLabel . uncurry (opeCoeffGeneric_ T phi r)) $
  case (parity, j) of
    (ParityEven, AnInteger l) | l >= 2 -> [(vec (TSOParityEven l), vec (STOParityEven l))]
    (ParityOdd,  AnInteger l) | l >= 2 -> [(vec (TSOParityOdd l),  vec (STOParityOdd l))]
    _ -> []

mapOpT :: ExternalOp s -> Maybe TTTT3d.ExternalOp
mapOpT T = Just TTTT3d.T
mapOpT _ = Nothing

-- | OPE coefficients for <T T O>
opeTT
  :: (Fractional a, Eq a)
  => ConformalRep Delta
  -> Parity
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
opeTT r parity = map (mapOPEStructLabel TTOLabel . mapOPEOps mapOpT) $
  TTTT3d.opeTT r parity

-- | <phi phi T>, where phi is Sig or Eps
opeSST
  :: (Eq a, Fractional a, Reifies s ExternalDims)
  => ExternalOp s
  -> OPECoefficientExternal (ExternalOp s) TSMixedStruct a
opeSST phi =
  \o1 o2 o3 ->
    mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
    let ops = (o1,o2,o3)
    in if
      | ops == (T,phi,phi) -> so3 2 2
      | ops == (phi,T,phi) -> so3 2 2
      | ops == (phi,phi,T) -> so3 0 2
      | otherwise          -> 0

-- | Let us define the quantities
--
-- lambda_B = n_B / c_T^{3/2} = n_B / (n_B + n_F)^{3/2}
-- lambda_F = n_F / c_T^{3/2} = n_F / (n_B + n_F)^{3/2}
--
-- These are the <TTT> OPE coefficients in conventions where
-- <TT>=1. We can parametrize the stress tensor OPE coefficients (n_B,
-- n_F), or alternatively the quantities (c_T, theta) from
-- arXiv:1708.05718 in terms of lambda_B and lambda_F. Writing
-- everything in these variables has some nice properties.
--
-- Note that lambda_B + lambda_F = 1/c_T^{1/2}. Thus, the Ward
-- identities imply that
--
-- lambda_{s s T} = -Delta_s/c_T^{1/2} = -Delta_s (lambda_B + lambda_F)
--
-- The nice thing is that this is linear in (lambda_B,
-- lambda_F). Thus, all OPE coefficients (in conventions where <OO>=1)
-- are linear in lambda_B, lambda_F, and the conformal block expansion
-- is quadratic in them.
--
-- The coefficient lambda_B multiplies the structures
--
-- <TTT>_B and -Delta_s <ssO>_{J=2}
--
-- Thus, we can define the OPECoefficient for lambda_B by opeTTT -
-- deltaSig * opeSST. (Similarly for lambda_F.) Overall, the effect of
-- the Ward identity is to "shift" the three-point structures for
-- lambda_B/F by an amount proportional to <ssO>_{J=2}. This should
-- generalize to arbitrary external operators.

-- | The structure -Delta_phi <phi phi T>
wardSST
  :: (Eq a, Fractional a, Reifies s ExternalDims)
  => ExternalOp s
  -> OPECoefficientExternal (ExternalOp s) TSMixedStruct a
wardSST phi = scaleOPECoeffExt (-(fromRational (rep phi).delta)) (opeSST phi)

lambda_B, lambda_F
  :: forall a s . (Eq a, Fractional a)
  => OPECoefficientExternal (ExternalOp s) TSMixedStruct a
lambda_B = mapOPEExtStructLabel TTOLabel $ mapOPEExtOps mapOpT $ fst $ fromV $ TTTT3d.opeTTT
lambda_F = mapOPEExtStructLabel TTOLabel $ mapOPEExtOps mapOpT $ snd $ fromV $ TTTT3d.opeTTT

-- | The ope coefficient <TTE>, normalized as in
-- https://arxiv.org/abs/2305.08914
lambdaTTEps
  :: forall a s . (Eq a, Fractional a, Reifies s ExternalDims)
  => OPECoefficientExternal (ExternalOp s) TSMixedStruct a
lambdaTTEps o1 o2 o3 =
  mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
  case (o1,o2,o3) of
    -- Automatically generated by toHaskell.nb on Sat 22 Jul 2023 22:23:56
    (Eps,T,T) -> 6/(dEps*(2 + dEps)) *^ vec (TSOLabel (STOParityEven 2))
    (T,Eps,T) -> 6/(dEps*(2 + dEps)) *^ vec (TSOLabel (TSOParityEven 2))
    (T,T,Eps) -> (-16*dEps)/(2 + dEps) *^ vec (TTOLabel ScalarParityEven)
    _         -> 0
  where
    dEps = fromRational (rep (Eps @s)).delta
