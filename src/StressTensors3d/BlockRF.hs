{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}

-- | A 'BlockRF' is a rational function that can multiply a block. We
-- impose that the rational function have a denominator of the form
-- \prod_i (x - a_i), where the poles a_i (which can have nontrivial
-- multiplicities) are 'Rational'. The numerator can be a general
-- Polynomial.

module StressTensors3d.BlockRF
  ( BlockRF(..)
  , constant
  , fromPolynomial
  , fromCoeffsPoles
  , mulDampedRational
  , shift
  , eval
  ) where

import Bootstrap.Math.DampedRational (DampedRational (..), One)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.Polynomial     (Polynomial)
import Bootstrap.Math.Polynomial     qualified as Polynomial
import Data.Functor.Identity         (Identity (..))
import Data.MultiSet                 qualified as MultiSet

newtype BlockRF a =  BlockRF (DampedRational (One a) Identity a)
  deriving newtype (Eq, Ord, Show)

-- | Convert a constant to a BlockRF
constant :: (Eq a, Num a) => a -> BlockRF a
constant = BlockRF . DR.constant . Identity

-- | Convert a 'Polynomial' to a 'BlockRF'
fromPolynomial :: Polynomial a -> BlockRF a
fromPolynomial = BlockRF . DR.fromPol . Identity

-- | Construct a 'BlockRF' from a list of coefficients for the
-- polynomial in the numerator and an association list of poles and
-- their degrees.
fromCoeffsPoles :: (Eq a, Num a) => [a] -> [(Rational, Int)] -> BlockRF a
fromCoeffsPoles numeratorCoeffs poles = BlockRF $ DampedRational
  { numerator = Identity $ Polynomial.fromList numeratorCoeffs
  , poles = MultiSet.fromOccurList poles
  }

-- | Shift 'x -> x+r'
shift :: (Eq a, Floating a) => Rational -> BlockRF a -> BlockRF a
shift r (BlockRF dr) = BlockRF $ DR.shift r dr

-- | Evaluate a 'BlockRF' at the given value.
eval :: RealFloat a => Rational -> BlockRF a -> a
eval x (BlockRF dr) = runIdentity $ DR.evalCancelPoleZeros x dr

instance (Fractional a, Eq a) => Num (BlockRF a) where
  (BlockRF dr1) + (BlockRF dr2) = BlockRF $ DR.addDampedRational dr1 dr2
  (BlockRF dr1) * (BlockRF dr2) = BlockRF $ DampedRational
    { numerator = numerator dr1 * numerator dr2
    , poles     = MultiSet.union (poles dr1) (poles dr2)
    }
  negate (BlockRF dr) = BlockRF $ DR.mapNumerator negate dr
  abs    (BlockRF dr) = BlockRF $ DR.mapNumerator abs dr
  signum (BlockRF dr) = BlockRF $ DR.mapNumerator signum dr
  fromInteger = constant . fromInteger

-- | Multiply a 'BlockRF a' times a 'DampedRational a'
mulDampedRational
  :: (Num a, Eq a, Functor f)
  => BlockRF a
  -> DampedRational base f a
  -> DampedRational base f a
mulDampedRational (BlockRF dr) b = DampedRational
  { numerator = fmap (* (dr.numerator.runIdentity)) b.numerator
  , poles     = MultiSet.union dr.poles b.poles
  }
