{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

-- | A 'CompositeBlock' is a linear combination of Blocks3d blocks,
-- built from 3-point structures that are linear combinations of SO3
-- structures with coefficients that are rational functions of Delta
-- (represented by a 'BlockRF'). The 3-point strucures of a
-- 'CompositeBlock t' are labeled by a type 't'. 't' should be an
-- instance of 'ToSO3Structs', so that we can turn a 't' three-point
-- structure into a concrete linear combination of SO3 structures.

module StressTensors3d.CompositeBlock
  ( CompositeBlock(..)
  , CompositeBlockKey(..)
  , SO3StructVar
  , ToSO3Structs (..)
  , threePtSO3_to_B3dSO3
  , compositeBlockBuildLink
  , readCompositeBlockTable
  , writeCompositeBlockTable
  ) where

import Blocks                        (Block (..), BlockBase, BlockFetchContext,
                                      BlockTableParams, ContinuumBlock (..),
                                      Coordinate (..), Delta (..),
                                      Derivative (..), HasBlocks,
                                      IsolatedBlock (..), KnownCoordinate)
import Blocks.Blocks3d               (Block3dParams, ConformalRep (..),
                                      Q4Struct (..), SO3Struct (..))
import Blocks.Blocks3d               qualified as B3d
import Blocks.Blocks3d.ReadTable     (BlockTable (..), DerivMap, deltaMinusX,
                                      readBlockTable)
import Blocks.Blocks3d.Types         (BlockTableKey)
import Blocks.ScalarBlocks.Types     (FourRhoCrossing (..))
import Bootstrap.Build               (BuildLink (..), ComputeDependencies,
                                      FetchConfig (..), FetchT, Fetches (..),
                                      dependencies, runFetchT)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.FreeVect       (FreeVect)
import Bootstrap.Math.FreeVect       qualified as FreeVect
import Bootstrap.Math.Polynomial     (Polynomial (..))
import Bootstrap.Math.VectorSpace    (VectorSpace (..))
import Bootstrap.Math.VectorSpace    qualified as VS
import Control.DeepSeq               (NFData, force)
import Control.Monad.IO.Class        (MonadIO, liftIO)
import Data.Binary                   (Binary)
import Data.Binary                   qualified as Binary
import Data.Functor.Compose          (Compose (..))
import Data.Map.Strict               qualified as Map
import Data.Proxy                    (Proxy (..))
import Data.Reflection               (reflect)
import Data.Tagged                   (Tagged)
import GHC.Generics                  (Generic, K1 (..), M1 (..), Rep, from,
                                      (:+:) (..))
import Hyperion                      (Dict (..), Job, Static (..), cAp, cPure,
                                      remoteEval)
import Hyperion.Bootstrap.Bound      (BigFloat, SDPFetchValue)
import Hyperion.Bootstrap.Util       (ToPath (..), mkTmpFilePath,
                                      mapConcurrentlyMonitored, setShuffled)
import Hyperion.Log                  qualified as Log
import Hyperion.Util                 (minute)
import Linear.V2                     (V2 (..))
import Numeric.Rounded               (reifyPrecision)
import StressTensors3d.BlockRF       (BlockRF)
import StressTensors3d.BlockRF       qualified as BlockRF
import StressTensors3d.ThreePtStruct (ThreePtStruct (..), mapLabel)
import StressTensors3d.ToTeX         (ToTeX (..))
import System.Directory              (createDirectoryIfMissing, doesFileExist,
                                      renameFile)
import System.FilePath               (takeDirectory)
import Type.Reflection               (Typeable)

newtype CompositeBlock t = CompositeBlock (Block (ThreePtStruct Delta t) Q4Struct)
  deriving newtype (Eq, Ord, Show, Binary, ToTeX)

-- | In a 'CompositeBlockKey', we replace 'Delta' with () to indicate
-- that Delta is an undetermined variable (from the point of view of
-- the block table). This ensures that blocks don't get re-computed
-- when only Delta changes.
data CompositeBlockKey t = CompositeBlockKey
  { block      :: Block (ThreePtStruct () t) Q4Struct
  , coordinate :: Coordinate
  , params     :: Block3dParams
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Binary)

instance (Typeable t, Static (Binary t)) => Static (Binary (CompositeBlockKey t)) where
  closureDict = static (\Dict -> Dict) `cAp` closureDict @(Binary t)

-- | The type of a computed block
type BlockDR a = DR.DampedRational (FourRhoCrossing a) DerivMap a

-- | An SO3Struct with Delta left unspecified. Here, () represents an
-- unspecified variable, hence "Var" in the name of the type.
type SO3StructVar = SO3Struct Rational Rational ()

-- | A class for converting the structure 't' to a linear combination
-- of SO3StructVar's times polynomials in Delta.
class ToSO3Structs t where
  toSO3Structs :: (Floating a, Eq a) => ThreePtStruct () t -> FreeVect SO3StructVar (BlockRF a)

  default toSO3Structs
    :: (Generic t, GToSO3Structs (Rep t), Floating a, Eq a)
    => ThreePtStruct () t
    -> FreeVect SO3StructVar (BlockRF a)
  toSO3Structs t = gtoSO3Structs (t { label = from t.label })

-- | A helper class for constructing instances of ToSO3Structs via
-- Generics. See this tutorial for details:
-- https://wiki.haskell.org/GHC.Generics
class GToSO3Structs f where
  gtoSO3Structs :: (Floating a, Eq a) => ThreePtStruct () (f b) -> FreeVect SO3StructVar (BlockRF a)

instance (GToSO3Structs f, GToSO3Structs g) => GToSO3Structs (f :+: g) where
  gtoSO3Structs t = case t.label of
    L1 l -> gtoSO3Structs $ t { label = l }
    R1 l -> gtoSO3Structs $ t { label = l }

instance (GToSO3Structs f) => GToSO3Structs (M1 i c f) where
  gtoSO3Structs = gtoSO3Structs . mapLabel unM1

instance (ToSO3Structs t) => GToSO3Structs (K1 i t) where
  gtoSO3Structs = toSO3Structs . mapLabel unK1

-- TODO: move to blocks-3d, or better yet implement SO3Struct in terms
-- of ThreePtStruct.
threePtSO3_to_B3dSO3 :: ThreePtStruct a B3d.SO3StructLabel -> B3d.SO3Struct Rational Rational a
threePtSO3_to_B3dSO3 (ThreePtStruct o1 o2 o3 (B3d.SO3StructLabel j12 j123)) = B3d.SO3Struct o1 o2 o3 j12 j123

instance ToSO3Structs B3d.SO3StructLabel where
  toSO3Structs s = FreeVect.vec (threePtSO3_to_B3dSO3 s)

instance Static (ToSO3Structs B3d.SO3StructLabel) where closureDict = static Dict

type instance SDPFetchValue a (CompositeBlockKey t)    = BlockDR a
type instance BlockFetchContext (CompositeBlock t) a m = Fetches (CompositeBlockKey t) (BlockDR a) m
type instance BlockTableParams (CompositeBlock t)      = Block3dParams
type instance BlockBase (CompositeBlock t) a           = FourRhoCrossing a

-- | A newtype wrapper that defines a VectorSpace instance for BlockDR
newtype BlockDRVec a = BlockDRVec { unBlockDRVec :: BlockDR a }

instance VectorSpace BlockDRVec where
  type IsBaseField BlockDRVec a = (Eq a, Fractional a, NFData a)
  zero = BlockDRVec (DR.fromPol Map.empty)
  scale a (BlockDRVec b) = BlockDRVec $ DR.mapNumerator (fmap (scale a)) b
  add (BlockDRVec b1) (BlockDRVec b2) = BlockDRVec (addBlockDR b1 b2)

-- | Add two BlockDR's. We 'force' the result so that BlockDR's can be
-- used as strict accumulators in a fold. This avoids memory leaks
-- when summing together a large number of BlockDR's, as happens when
-- we are constructing a CompositeBlock. In practice, this creates 10x+
-- improvements in memory usage when building stress tensor blocks.
addBlockDR :: forall a . (Fractional a, Eq a, NFData a) => BlockDR a -> BlockDR a -> BlockDR a
addBlockDR b1 b2 = force $ DR.mapNumerator sumV2DerivMap b12
  where
    b12 :: DR.DampedRational (FourRhoCrossing a) (Compose V2 DerivMap) a
    b12 = DR.sequence (V2 b1 b2)
    sumV2DerivMap :: Compose V2 DerivMap (Polynomial a) -> DerivMap (Polynomial a)
    sumV2DerivMap (Compose (V2 p1 p2)) = Map.unionWith (+) p1 p2

-- | Strip off the 'ConformalRep Delta' from the 3-point structures
stripBlock :: CompositeBlock t -> Block (ThreePtStruct () t) Q4Struct
stripBlock (CompositeBlock (Block s12 s43 f)) = Block (stripStruct s12) (stripStruct s43) f
  where
    stripStruct (ThreePtStruct o1 o2 o3 t) = ThreePtStruct o1 o2 (stripRep o3) t
    stripRep (ConformalRep _ j) = ConformalRep () j

-- | Given a map 'expandStruct' from t to a linear combination of
-- t''s, expand the Block for t into a linear combination of Block's
-- for t' by expanding both of its three-point structures
expandBlock :: (Num a, Eq a, Ord t', Ord f) => (t -> FreeVect t' a) -> Block t f -> FreeVect (Block t' f) a
expandBlock expandStruct b =
  FreeVect.multiplyWith (\s12 s43 -> Block s12 s43 b.fourPtFunctional)
  (expandStruct b.struct12)
  (expandStruct b.struct43)

-- | Fetch the block as a DampedRational of x, where
--
-- Delta = B3d.unitarityBound j + x
--
fetchCompositeBlock
  :: forall a f t . ( Floating a, Eq a, Applicative f
                    , Fetches BlockTableKey (BlockTable a) f
                    , ToSO3Structs t, NFData a)
  => CompositeBlockKey t
  -> f (BlockDR a)
fetchCompositeBlock b =
  -- We use the VectorSpace instance for (Compose f v), where f is an
  -- Applicative, and v is a VectorSpace, here given by
  -- BlockDRVec. VS.sum is a foldl involving pairwise add's. When f
  -- gets specialized to IO, this means we will fetch a block one at a
  -- time and add it onto an accumulator.
  fmap unBlockDRVec $ getCompose $ VS.sum $ do
  -- Expand the block in SO3 structures and group terms by their
  -- BlockTableKey
  (key, keyTerms) <- Map.toList $
    FreeVect.groupTermsBy toKey $
    expandBlock toSO3Structs b.block
  -- For each key, fetch the corresponding block table and look up the
  -- data for each term in keyTerms in the block table
  pure $ Compose $ do
    blockTable <- fetch key
    pure $ keyTerms FreeVect.// \block3dVar coeff ->
      let
        deltaUnitarity = B3d.unitarityBound (B3d.internalRep block3dVar).spin
        -- Convert the coefficient to a function of x
        coeffOfX = BlockRF.shift deltaUnitarity coeff
        blockDR =
          -- Convert the block to a function of x. Since
          -- blockTable.deltaMinusX is expected to be deltaUnitarity,
          -- this should be a no-op.
          DR.shift (deltaUnitarity - blockTable.deltaMinusX) $
          B3d.blockTableToBlockDR b.coordinate block3dVar blockTable
      in
        BlockDRVec $ BlockRF.mulDampedRational coeffOfX blockDR
  where
    toKey :: Block SO3StructVar Q4Struct -> BlockTableKey
    toKey = B3d.blockToBlockTableKey' b.coordinate b.params

-- | Make the given 'CompositeBlockKey' into a DampedRational by
-- reading all the appropriate Block3d's, and assembling them.
makeCompositeBlock
  :: forall a t . (Floating a, Eq a, ToSO3Structs t, NFData a)
  => FilePath
  -> CompositeBlockKey t
  -> IO (BlockDR a)
makeCompositeBlock blockTableDir b =
  runFetchT (fetchCompositeBlock b
             :: FetchT '[ '(BlockTableKey, BlockTable a) ] IO (BlockDR a)) cfg
  where
    cfg = readBlockTableLog :&: FetchNil
    readBlockTableLog table = do
      Log.info "CompositeBlock: Reading block table" (B3d.blockTableFilePath blockTableDir table)
      readBlockTable blockTableDir table

-- | Compute the dependencies of the given 'CompositeBlockKey' in terms
-- of 'B3d.BlockTableKey's
compositeBlockDependencies
  :: ToSO3Structs t
  => CompositeBlockKey t
  -> [BlockTableKey]
compositeBlockDependencies b =
  dependencies (fetchCompositeBlock b
                :: ComputeDependencies '[ '( BlockTableKey, BlockTable Double ) ] (BlockDR Double))

-- | Make the given 'CompositeBlockKey' into a DampedRational and write
-- it to a file. We pass in an explicit dictionary instead of using
-- typeclass constraints for ease of use with 'remoteEval' below.
writeCompositeBlockTable
  :: MonadIO m
  => Dict (Binary t, ToSO3Structs t, Typeable t, Show t, ToPath (CompositeBlockKey t))
  -> FilePath              -- ^ Directory for block tables
  -> CompositeBlockKey t -- ^ Block tables to generate
  -> m ()
writeCompositeBlockTable Dict blockTableDir b =
  liftIO $ 
  reifyPrecision b.params.precision $ \(_ :: Proxy p) -> do
  let
    file = toPath blockTableDir b
  createDirectoryIfMissing True (takeDirectory file)
  tmpfile <- mkTmpFilePath file
  Log.info "Writing CompositeBlockTable to temporary file" (b, tmpfile)
  block :: BlockDR (BigFloat p) <- makeCompositeBlock blockTableDir b
  Binary.encodeFile tmpfile block
  Log.info "Moving temporary to permanent file" (b,tmpfile,file)
  renameFile tmpfile file


-- | Read the given 'CompositeBlockKey' from a file
readCompositeBlockTable
  :: (Floating a, Eq a, Binary a, MonadIO m, ToPath (CompositeBlockKey t))
  => FilePath
  -> CompositeBlockKey t
  -> m (BlockDR a)
readCompositeBlockTable blockTableDir key =
  liftIO $
  Binary.decodeFileOrFail (toPath blockTableDir key) >>= \case
    Right m       -> pure m
    Left (_, err) -> error $ "readCompositeBlockTable: " ++ err

-- | A 'BuildLink' for 'CompositeBlockKey's in terms of
-- 'B3d.BlockTableKey's
compositeBlockBuildLink
  :: ( Typeable t, Static (Binary t), Static (ToSO3Structs t), Static (Show t)
     , Static (ToPath (CompositeBlockKey t))
     )
  => FilePath
  -> BuildLink Job (CompositeBlockKey t) BlockTableKey
compositeBlockBuildLink blockTableDir = BuildLink
  { buildDeps = compositeBlockDependencies
  , checkCreated = liftIO . doesFileExist . toPath blockTableDir
  , buildAll =
    mapConcurrentlyMonitored "CompositeBlocks" (2*minute) remoteWriteCompositeBlockTable .
    setShuffled
  }
  where
    -- TODO: Change to handle chunks of larger size
    remoteWriteCompositeBlockTable k = remoteEval $
      static writeCompositeBlockTable
      `cAp` closureDict
      `cAp` cPure blockTableDir
      `cAp` cPure k

-- | Return a DampedRational of x for the given vector of derivatives,
-- along with the the appropriate xShift for the given block.
--
-- Note that the result of fetching a CompositeBlockKey is a
-- DampedRational of x where
--
-- Delta = unitarityBound j + x
--
-- Thus, we have
--
-- xShift = Delta - unitarityBound j
--
getCompositeBlockAndShift
  :: forall t p m a c v . (HasBlocks (CompositeBlock t) p m a, Functor v, Foldable v, KnownCoordinate c)
  => v (Derivative c)
  -> CompositeBlock t
  -> Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
getCompositeBlockAndShift derivVec b = Compose $ pure $ do
  block <- fetch (CompositeBlockKey (stripBlock b) coord params)
  pure $ (xShift, B3d.blockDRToBlockVector derivVec block)
  where
    coord = B3d.derivVecCoordinate derivVec
    params = reflect @p Proxy
    -- Assume the internal rep is the same for both structures
    internalRep = case b of
      CompositeBlock (Block s12 _ _) -> s12.rep3
    xShift = B3d.fixedDelta internalRep - B3d.unitarityBound internalRep.spin

getCompositeBlockContinuum
  :: (HasBlocks (CompositeBlock t) p m a, Functor v, Foldable v, Floating a, Eq a, KnownCoordinate c)
  => v (Derivative c)
  -> CompositeBlock t
  -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
getCompositeBlockContinuum derivVec b = uncurry DR.shift <$> getCompositeBlockAndShift derivVec b

getCompositeBlockIsolated
  :: (HasBlocks (CompositeBlock t) p m a, Functor v, Foldable v, RealFloat a, KnownCoordinate c)
  => v (Derivative c)
  -> CompositeBlock t
  -> Compose (Tagged p) m (v a)
getCompositeBlockIsolated derivVec b = eval <$> getCompositeBlockAndShift derivVec b
  where
    eval (x, v) = DR.evalCancelPoleZeros (fromRational x) v

instance (RealFloat a, KnownCoordinate c) => IsolatedBlock (CompositeBlock t) (Derivative c) a where
  getBlockIsolated = getCompositeBlockIsolated

instance (RealFloat a, KnownCoordinate c) => ContinuumBlock (CompositeBlock t) (Derivative c) a where
  getBlockContinuum = getCompositeBlockContinuum
